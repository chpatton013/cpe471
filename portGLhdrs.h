#ifndef __PORT_GL_HDRS__
#define __PORT_GL_HDRS__

   #ifdef __APPLE__
      #include <OpenGL/gl.h>
      #include <OpenGL/glu.h>
      #include <GLUT/glut.h>
   #else
      #include <GL/gl.h>
      #include <GL/glu.h>
      #include <GL/glut.h>
   #endif

#endif
