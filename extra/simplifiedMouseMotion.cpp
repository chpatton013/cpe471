#include <GL/glut.h>
#include <math.h>

// some function prototypes
void display(void);

// set up the light sources for the scene
// a directional light source from directly behind
GLfloat lightDir[] = {0.0, 0.0, 5.0, 0.0};
GLfloat diffuseComp[] = {1.0, 1.0, 1.0, 1.0};
GLfloat diffuseColor[] = {1.0, 1.0, 1.0, 0.5};

int winWidth, winHeight;

float angle = 0.0, axis[3];
bool trackballMove = false;
bool zoomState = false;
bool shiftState = false;

GLfloat lightXform[4][4] = {
   {1.0, 0.0, 0.0, 0.0},
   {0.0, 1.0, 0.0, 0.0},
   {0.0, 0.0, 1.0, 0.0},
   {0.0, 0.0, 0.0, 1.0}
};
GLfloat objectXform[4][4] = {
   {1.0, 0.0, 0.0, 0.0},
   {0.0, 1.0, 0.0, 0.0},
   {0.0, 0.0, 1.0, 0.0},
   {0.0, 0.0, 0.0, 1.0}
};
GLfloat *trackballXform = (GLfloat *)objectXform;

// initial viewer position
static GLdouble modelTrans[] = {0.0, 0.0, -5.0};

//---------------------------------------------------------
//   Set up the view

void setUpView() {
   // this code initializes the viewing transform
   glLoadIdentity();

   // moves viewer along coordinate axes
   gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

//----------------------------------------------------------
//  Set up model transform

void setUpModelTransform() {
   // moves model along coordinate axes
   glTranslatef(modelTrans[0], modelTrans[1], modelTrans[2]);
}

//----------------------------------------------------------
//  Set up the light

void setUpLight() {
   glLightfv(GL_LIGHT0, GL_POSITION, lightDir);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseComp);
}

//--------------------------------------------------------
//  Set up the objects

void drawObjs() {
   glPushMatrix();

   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);

   glMatrixMode(GL_MODELVIEW);

   glTranslatef(0, 0, 1);
   glutSolidCube(2.0);

   glPopMatrix();
}

//-----------------------------------------------------------
//  Callback functions

void reshapeCallback(int w, int h) {
   // from Angel, p.562

   glViewport(0,0,w,h);
   winWidth = w;
   winHeight = h;

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(60.0, 1.0, 0.1, 50.0);

   glMatrixMode(GL_MODELVIEW);
}

float lastPos[3] = {0.0, 0.0, 0.0};
int startX, startY;

void trackball_ptov(int x, int y, int width, int height, float v[3]) {
   float v0, v1, v2, sqr, root, scale;

   v0 = (2 * x - width) / width;
   v1 = (height - 2 * y) / height;

   sqr = v0 * v0 + v1 * v1;
   root = sqrt(sqr);

   v2 = cos((M_PI / 2.0) * ((root < 1.0) ? root : 1.0));

   scale = 1.0 / sqrt(sqr + v2 * v2);

   v[0] = v0 * scale;
   v[1] = v1 * scale;
   v[2] = v2 * scale;
}

void mouseMotion(int x, int y) {
   float curPos[3], dx, dy, dz;

   if (zoomState == false && shiftState == false) {

      trackball_ptov(x, y, winWidth, winHeight, curPos);

      dx = curPos[0] - lastPos[0];
      dy = curPos[1] - lastPos[1];
      dz = curPos[2] - lastPos[2];

      if (dx||dy||dz) {
         angle = 90.0 * sqrt(dx*dx + dy*dy + dz*dz);

         axis[0] = lastPos[1]*curPos[2] - lastPos[2]*curPos[1];
         axis[1] = lastPos[2]*curPos[0] - lastPos[0]*curPos[2];
         axis[2] = lastPos[0]*curPos[1] - lastPos[1]*curPos[0];

         lastPos[0] = curPos[0];
         lastPos[1] = curPos[1];
         lastPos[2] = curPos[2];
      }

   }
   else if (zoomState == true) {
      curPos[1] = y;
      dy = curPos[1] - lastPos[1];

      if (dy) {
         modelTrans[2] += dy * 0.01;
         lastPos[1] = curPos[1];
      }
   }
   else if (shiftState == true) {
      curPos[0] = x;
      curPos[1] = y;
      dx = curPos[0] - lastPos[0];
      dy = curPos[1] - lastPos[1];

      if (dx) {
         modelTrans[0] += dx * 0.01;
         lastPos[0] = curPos[0];
      }
      if (dy) {
         modelTrans[1] -= dy * 0.01;
         lastPos[1] = curPos[1];
      }
   }
   glutPostRedisplay( );

}

void startMotion(long time, int button, int x, int y) {
   startX = x; startY = y;
   trackball_ptov(x, y, winWidth, winHeight, lastPos);
   trackballMove = true;
}

void stopMotion(long time, int button, int x, int y) {
   if (startX == x && startY == y) {
      angle = 0.0;
      trackballMove = false;
   }
}

void mouseCallback(int button, int state, int x, int y) {

   switch (button) {
      case GLUT_LEFT_BUTTON:
         trackballXform = (GLfloat *)objectXform;
         break;
      case GLUT_RIGHT_BUTTON:
      case GLUT_MIDDLE_BUTTON:
         trackballXform = (GLfloat *)lightXform;
         break;
   }
   switch (state) {
      case GLUT_DOWN:
         if (button == GLUT_RIGHT_BUTTON) {
            zoomState = true;
            lastPos[1] = y;
         }
         else if (button == GLUT_MIDDLE_BUTTON) {
            shiftState = true;
            lastPos[0] = x;
            lastPos[1] = y;
         }
         else startMotion(0, 1, x, y);
         break;
      case GLUT_UP:
         trackballXform = (GLfloat *)lightXform; // turns off mouse effects
         if (button == GLUT_RIGHT_BUTTON) {
            zoomState = false;
         }
         else if (button == GLUT_MIDDLE_BUTTON) {
            shiftState = false;
         }
         else stopMotion(0, 1, x, y);
         break;
   }
}

void keyCallback(unsigned char key, int x, int y) {

   glutPostRedisplay();
}


//---------------------------------------------------------
//  Main routines

void display (void) {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   setUpView();
   setUpLight();
   setUpModelTransform();

   if (trackballMove) {
      glPushMatrix();
      glLoadIdentity();
      glRotatef(angle, axis[0], axis[1], axis[2]);
      glMultMatrixf((GLfloat *) trackballXform);
      glGetFloatv(GL_MODELVIEW_MATRIX, trackballXform);
      glPopMatrix();
   }

   glPushMatrix();
   glMultMatrixf((GLfloat *) objectXform);
   drawObjs();
   glPopMatrix();

   glutSwapBuffers();
}

// create a double buffered 500x500 pixel color window
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Mouse motion example");

   glEnable(GL_DEPTH_TEST);
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glutDisplayFunc(display);
   glutReshapeFunc(reshapeCallback);
   glutKeyboardFunc(keyCallback);
   glutMouseFunc(mouseCallback);
   glutMotionFunc(mouseMotion);

	glutMainLoop();

	return 0;
}

