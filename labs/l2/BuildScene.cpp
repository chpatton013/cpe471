/*
 *  Lab exercise 2.0
 *  CPE 471, Computer Graphics
 *  Copyright 2005 Chris Buckalew
 *
 *--------------------------------------------------------------
 *  This code contains one OpenGL primitive.  Use this and
 *  other primitives (including from last lab) to build a scene.
 *  Rearrange and add to this scene to make a final scene
 *  that contains:
 *  1) at the far left, a tall thin yellow cone,
 *  2) in the middle, a short wide red teapot,
 *  3) at the right, a rather small cyan sphere
 *  4) above the middle, a green box that's 2x3x5,
 *  5) below the middle, a blue pyramid that's 5 units high
 *  6) off to the side, a magenta wireframe torus
 *
 *  You will likely need to look up GLUT documentation (GL
 *  Utility Library) - just google "glut documentation".
 *  This is the information you need for parameters to the
 *  different shape function calls.
 *
 *  You will need to use transforms to move objects around.
 *  The three types available are Translate (moves things
 *  around), Scale (bigger or smaller), and Rotate.
 *  Experiment with these transformations to see basically
 *  how they work.
 *
 *------------------------------------------------------------*/

#ifdef __APPLE__
#include "GLUT/glut.h"
#include <OPENGL/gl.h>
#endif
#ifdef __unix__
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// some function prototypes
void display(void);
void normalize(float[3]);
void normCrossProd(float[3], float[3], float[3]);

// initial viewer position
static GLdouble modelTrans[] = {0.0, 0.0, -5.0};
// initial model angle
static GLfloat theta[] = {0.0, 0.0, 0.0};
static float thetaIncr = 5.0;

// animation transform variables
static GLdouble translate[3] = {-10.0, 0.0, 0.0};

//---------------------------------------------------------
//   Set up the view

void setUpView() {
   // this code initializes the viewing transform
   glLoadIdentity();

   // moves viewer along coordinate axes
   gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

   // move the view back some relative to viewer[] position
   glTranslatef(0.0f,0.0f, -8.0f);

   // rotates view
   glRotatef(0, 1.0, 0.0, 0.0);
   glRotatef(0, 0.0, 1.0, 0.0);
   glRotatef(0, 0.0, 0.0, 1.0);

   return;
}

//----------------------------------------------------------
//  Set up model transform

void setUpModelTransform() {

   // moves model along coordinate axes
   glTranslatef(modelTrans[0], modelTrans[1], modelTrans[2]);

   // rotates model
   glRotatef(theta[0], 1.0, 0.0, 0.0);
   glRotatef(theta[1], 0.0, 1.0, 0.0);
   glRotatef(theta[2], 0.0, 0.0, 1.0);


}

//----------------------------------------------------------
//  Set up the light

void setUpLight() {
   // set up the light sources for the scene
   // a directional light source from directly behind
   GLfloat lightDir[] = {0.0, 0.0, 5.0, 0.0};
   GLfloat diffuseComp[] = {1.0, 1.0, 1.0, 1.0};

   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glLightfv(GL_LIGHT0, GL_POSITION, lightDir);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseComp);

   return;
}

//--------------------------------------------------------
//  Set up the objects

void drawCone() {
   glPushMatrix();

   GLfloat diffuseColor[] = {1.0, 1.0, 0.0, 1.0};
   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);

   glMatrixMode(GL_MODELVIEW);

   glTranslatef(-10, 0, 0);
   glScalef(1, 10, 1);
   glRotatef(-90, 1, 0, 0);
   glutSolidCone(1.0, 1.0, 10, 10);

   glPopMatrix();
}

void drawTeapot() {
   glPushMatrix();

   GLfloat diffuseColor[] = {1.0, 0.0, 0.0, 1.0};
   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);

   glMatrixMode(GL_MODELVIEW);

   glScalef(3, 1, 1.5);
   glutSolidTeapot(1.0);

   glPopMatrix();
}

void drawSphere() {
   glPushMatrix();

   GLfloat diffuseColor[] = {0.0, 1.0, 1.0, 1.0};
   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);

   glMatrixMode(GL_MODELVIEW);

   glTranslatef(10, 0, 0);
   glScalef(0.5, 0.5, 0.5);
   glutSolidSphere(1.0, 10, 10);

   glPopMatrix();
}

void drawBox() {
   glPushMatrix();

   GLfloat diffuseColor[] = {0.0, 1.0, 0.0, 1.0};
   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);

   glMatrixMode(GL_MODELVIEW);

   glTranslatef(0, 10, 0);
   glScalef(2, 3, 5);
   glutSolidCube(1.0);

   glPopMatrix();
}

void drawPyramid() {
   glPushMatrix();

   GLfloat diffuseColor[] = {0.0, 0.0, 1.0, 1.0};
   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);

   glMatrixMode(GL_MODELVIEW);

   glScalef(5, 5, 5);
   glTranslatef(-0.5, -2, -0.5);
   glRotatef(90, -1, 0, 0);

   float vertices[] = {
      0, 0, 0,    // bottom left
      1, 0, 0,    // bottom right
      1, 1, 0,    // top right
      0, 1, 0,    // bottom left
      0.5, 0.5, 1 // top
   };

   int indices[] = {
      0, 1, 2,    // bottom-right
      0, 2, 3,    // bottom-left
      0, 1, 4,    // front
      1, 2, 4,    // right
      2, 3, 4,    // back
      3, 0, 4     // left
   };

   float d1[3], d2[3], normal[3];

   glBegin(GL_TRIANGLES);

   for (int ndx1 = 0; ndx1 < 6; ++ndx1) {
      for (int ndx2 = 0; ndx2 < 3; ++ndx2) {
         d1[ndx2] = vertices[indices[ndx1 * 3 + 1] * 3 + ndx2] -
          vertices[indices[ndx1 * 3] * 3 + ndx2];
         d2[ndx2] = vertices[indices[ndx1 * 3 + 2] * 3 + ndx2] -
          vertices[indices[ndx1 * 3] * 3 + ndx2];
      }

      normCrossProd(d1, d2, normal);
      glNormal3fv(normal);

      glVertex3fv(&vertices[indices[ndx1 * 3] * 3]);
      glVertex3fv(&vertices[indices[ndx1 * 3 + 1] * 3]);
      glVertex3fv(&vertices[indices[ndx1 * 3 + 2] * 3]);
   }

   glEnd();

   glPopMatrix();
}

void drawTorus() {
   glPushMatrix();

   GLfloat diffuseColor[] = {1.0, 0.0, 0.5, 1.0};
   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);

   glMatrixMode(GL_MODELVIEW);

   glTranslatef(10, 10, 0);
   glutWireTorus(1.0, 2.0, 10, 10);

   glPopMatrix();
}

//-----------------------------------------------------------
//  Callback functions

void reshapeCallback(int w, int h) {
   // from Angel, p.562

   glViewport(0,0,w,h);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   if (w < h) {
      glFrustum(-2.0, 2.0, -2.0*(GLfloat) h / (GLfloat) w,
                2.0*(GLfloat) h / (GLfloat) w, 2.0, 200.0);
   }
   else {
      glFrustum(-2.0, 2.0, -2.0*(GLfloat) w / (GLfloat) h,
                2.0*(GLfloat) w / (GLfloat) h, 2.0, 200.0);
   }

   glMatrixMode(GL_MODELVIEW);
}

void mouseCallback(int button, int state, int x, int y) {
   // rotate camera
   GLint axis = 3;
   if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) axis = 0;
   if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) axis = 1;
   if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) axis = 2;
   if (axis < 3) {  // button ups won't change axis value from 3
      theta[axis] += thetaIncr;
      if (theta[axis] > 360.0) theta[axis] -= 360.0;
      display();
   }
}

void keyCallback(unsigned char key, int x, int y) {
   // move viewer with x, y, and z keys
   // capital moves in + direction, lower-case - direction
   if (key == 'x') modelTrans[0] -= 1.0;
   if (key == 'X') modelTrans[0] += 1.0;
   if (key == 'y') modelTrans[1] -= 1.0;
   if (key == 'Y') modelTrans[1] += 1.0;
   if (key == 'z') modelTrans[2] -= 1.0;
   if (key == 'Z') modelTrans[2] += 1.0;
   if (key == 'r') {
      theta[0] = 0.0; theta[1] = 0.0; theta[2] = 0.0;
   }
   if (key == '-') {
      thetaIncr = -thetaIncr;
   }
   if (key == '+') {
      if (thetaIncr < 0) thetaIncr = thetaIncr - 1.0;
      else               thetaIncr = thetaIncr + 1.0;
   }
   display();
}


//---------------------------------------------------------
//  Main routines

void display (void) {
   // this code executes whenever the window is redrawn (when opened,
   //   moved, resized, etc.
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   // set the viewing transform
   setUpView();

   // set up light source
   setUpLight();

   // start drawing objects
   setUpModelTransform();
   drawCone();
   drawTeapot();
   drawSphere();
   drawBox();
   drawPyramid();
   drawTorus();

   glutSwapBuffers();
}

// create a double buffered 500x500 pixel color window
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Building a Scene: Lab 2");
	glEnable(GL_DEPTH_TEST);
	glutDisplayFunc(display);
   glutReshapeFunc(reshapeCallback);
   glutKeyboardFunc(keyCallback);
   glutMouseFunc(mouseCallback);
	glutMainLoop();
	return 0;
}

//---------------------------------------------------------
//  Utility functions

void normalize(float v[3]) {
   // normalize v[] and return the result in v[]
   // from OpenGL Programming Guide, p. 58
   GLfloat d = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
   if (d == 0.0) {
      printf("zero length vector");
      return;
   }
   v[0] = v[0]/d; v[1] = v[1]/d; v[2] = v[2]/d;
}

void normCrossProd(float v1[3], float v2[3], float out[3]) {
   // cross v1[] and v2[] and return the result in out[]
   // from OpenGL Programming Guide, p. 58
   out[0] = v1[1]*v2[2] - v1[2]*v2[1];
   out[1] = v1[2]*v2[0] - v1[0]*v2[2];
   out[2] = v1[0]*v2[1] - v1[1]*v2[0];
   normalize(out);
}

