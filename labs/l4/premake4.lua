solution ('Lab4')
   configurations ({'Debug', 'Release'})
   language ('C++')
   files ({'./**.cpp', './**.cc', './**.h'})
   defines ({'GL_GLEXT_PROTOTYPES'})
   links ({'m', 'GL', 'GLU', 'glut'})

   configuration ('Debug')
      defines ({'DEBUG'})
      flags ({'Symbols'})

   configuration ('Release')
      defines ({'NDEBUG'})
      flags ({'Optimize'})

   project ('lab4')
      kind ('ConsoleApp')
