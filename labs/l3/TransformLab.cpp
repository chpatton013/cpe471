/*
 *  Christopher Patton
 *  Lab exercise 3.0
 *
 *--------------------------------------------------------------
 *  This code contains five examples of using transformations
 *  to place two OpenGL primitives, a cube and a cone, in
 *  different locations.
 *
 *  1) Create a big "X" made of two cubes in each arm, with
 *     points (cones) on each end.
 *  2) Make a circle of cubes - use a loop and successive
 *     transforms to place each cube.  Make sure the cubes are
 *     rotated so that the result looks sort of like a torus
 *     with a square profile.
 *  3) Draw a 3-level "sphere-flake".  Draw a large red sphere,
 *     surrounded by six smaller green spheres, in each of the
 *     +/- x, y, and z directions.  Surround each of these with
 *     six even smaller blue spheres in the same directions.
 *     This can be efficiently done using recursion or
 *     triple-nested loops.  DON'T do it with manual cut-and-paste.
 *
 *------------------------------------------------------------
 */

#include <GL/glut.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// some function prototypes
void display(void);
void normalize(float[3]);
void normCrossProd(float[3], float[3], float[3]);

// initial viewer position
static GLdouble modelTrans[] = {0.0, 0.0, -5.0};
// initial model angle
static GLfloat theta[] = {0.0, 0.0, 0.0};
static float thetaIncr = 5.0;

// animation transform variables
static GLdouble translate[3] = {-10.0, 0.0, 0.0};

static float red[] = {1.0, 0.0, 0.0, 1.0};
static float green[] = {0.0, 1.0, 0.0, 1.0};
static float blue[] = {0.0, 0.0, 1.0, 1.0};

//---------------------------------------------------------
//   Set up the view

void setUpView() {
   // this code initializes the viewing transform
   glLoadIdentity();

   // moves viewer along coordinate axes
   gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

   // move the view back some relative to viewer[] position
   glTranslatef(0.0f,0.0f, -8.0f);

   // rotates view
   glRotatef(0, 1.0, 0.0, 0.0);
   glRotatef(0, 0.0, 1.0, 0.0);
   glRotatef(0, 0.0, 0.0, 1.0);

   return;
}

//----------------------------------------------------------
//  Set up model transform

void setUpModelTransform() {

   // moves model along coordinate axes
   glTranslatef(modelTrans[0], modelTrans[1], modelTrans[2]);

   // rotates model
   glRotatef(theta[0], 1.0, 0.0, 0.0);
   glRotatef(theta[1], 0.0, 1.0, 0.0);
   glRotatef(theta[2], 0.0, 0.0, 1.0);


}

//----------------------------------------------------------
//  Set up the light

void setUpLight() {
   // set up the light sources for the scene
   // a directional light source from over the right shoulder
   GLfloat lightDir[] = {0.0, 0.0, 5.0, 0.0};
   GLfloat diffuseComp[] = {1.0, 1.0, 1.0, 1.0};

   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glLightfv(GL_LIGHT0, GL_POSITION, lightDir);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseComp);

   return;
}

//--------------------------------------------------------
//  Set up the objects

void drawArm() {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);
   glutSolidCube(4);

   glPushMatrix();
   glTranslatef(2, 0, 0);
   glRotatef(90, 0, 1, 0);
   glMaterialfv(GL_FRONT, GL_DIFFUSE, red);
   glutSolidCone(2, 4, 10, 10);
   glPopMatrix();
}

void drawX() {
   glRotatef(45, 0, 0, 1);

   glPushMatrix();

   glMaterialfv(GL_FRONT, GL_DIFFUSE, green);
   glutSolidCube(4);

   glPushMatrix();
   glTranslatef(4, 0, 0);
   drawArm();
   glPopMatrix();

   glPushMatrix();
   glRotatef(90, 0, 0, 1);
   glTranslatef(4, 0, 0);
   drawArm();
   glPopMatrix();

   glPushMatrix();
   glRotatef(180, 0, 0, 1);
   glTranslatef(4, 0, 0);
   drawArm();
   glPopMatrix();

   glPushMatrix();
   glRotatef(270, 0, 0, 1);
   glTranslatef(4, 0, 0);
   drawArm();
   glPopMatrix();

   glPopMatrix();
}

void drawCircle() {
   glPushMatrix();

   glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);

   for (int ndx = 0; ndx < 36; ++ndx) {
      glPushMatrix();

      glRotatef(10 * ndx, 0, 0, 1);
      glTranslatef(5, 0, 0);
      glutSolidCube(2);

      glPopMatrix();
   }

   glPopMatrix();
}

void drawSphereLevel3(double radius) {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);
   glutSolidSphere(radius, 10, 10);
}

void drawSphereLevel2(double radius) {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, green);
   glutSolidSphere(radius, 10, 10);

   glPushMatrix();
   glTranslatef(radius * 3, 0, 0);
   drawSphereLevel3(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-(radius * 3), 0, 0);
   drawSphereLevel3(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0, radius * 3, 0);
   drawSphereLevel3(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0, -(radius * 3), 0);
   drawSphereLevel3(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0, 0, radius * 3);
   drawSphereLevel3(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0, 0, -(radius * 3));
   drawSphereLevel3(radius / 2.0);
   glPopMatrix();
}

void drawSphereLevel1(double radius) {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, red);
   glutSolidSphere(radius, 10, 10);

   glPushMatrix();
   glTranslatef(radius * 3, 0, 0);
   drawSphereLevel2(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-(radius * 3), 0, 0);
   drawSphereLevel2(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0, radius * 3, 0);
   drawSphereLevel2(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0, -(radius * 3), 0);
   drawSphereLevel2(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0, 0, radius * 3);
   drawSphereLevel2(radius / 2.0);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0, 0, -(radius * 3));
   drawSphereLevel2(radius / 2.0);
   glPopMatrix();
}

void drawSphere() {
   glPushMatrix();
   drawSphereLevel1(3.0);
   glPopMatrix();
}

void drawExamples() {
   glPushMatrix();
   glTranslatef(-25, 0, 0);
   drawX();
   glPopMatrix();

   glPushMatrix();
   drawCircle();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(25, 0, 0);
   drawSphere();
   glPopMatrix();
}

//-----------------------------------------------------------
//  Callback functions

void reshapeCallback(int w, int h) {
   // from Angel, p.562

   glViewport(0,0,w,h);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   if (w < h) {
      glFrustum(-2.0, 2.0, -2.0*(GLfloat) h / (GLfloat) w,
                2.0*(GLfloat) h / (GLfloat) w, 2.0, 20.0);
   }
   else {
      glFrustum(-2.0, 2.0, -2.0*(GLfloat) w / (GLfloat) h,
                2.0*(GLfloat) w / (GLfloat) h, 2.0, 20.0);
   }

   glMatrixMode(GL_MODELVIEW);
}

void mouseCallback(int button, int state, int x, int y) {
   // rotate camera
   GLint axis = 3;
   if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) axis = 0;
   if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) axis = 1;
   if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) axis = 2;
   if (axis < 3) {  // button ups won't change axis value from 3
      theta[axis] += thetaIncr;
      if (theta[axis] > 360.0) theta[axis] -= 360.0;
      display();
   }
}

void keyCallback(unsigned char key, int x, int y) {
   // move viewer with x, y, and z keys
   // capital moves in + direction, lower-case - direction
   if (key == 'x') modelTrans[0] -= 1.0;
   if (key == 'X') modelTrans[0] += 1.0;
   if (key == 'y') modelTrans[1] -= 1.0;
   if (key == 'Y') modelTrans[1] += 1.0;
   if (key == 'z') modelTrans[2] -= 1.0;
   if (key == 'Z') modelTrans[2] += 1.0;
   if (key == 'r') {
      theta[0] = 0.0; theta[1] = 0.0; theta[2] = 0.0;
   }
   if (key == '-') {
      thetaIncr = -thetaIncr;
   }
   if (key == '+') {
      if (thetaIncr < 0) thetaIncr = thetaIncr - 1.0;
      else               thetaIncr = thetaIncr + 1.0;
   }
   display();
}


//---------------------------------------------------------
//  Main routines

void display (void) {
   // this code executes whenever the window is redrawn (when opened,
   //   moved, resized, etc.
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   // set the viewing transform
   setUpView();

   // set up light source
   setUpLight();

   // start drawing objects
   setUpModelTransform();
   drawExamples();

   glutSwapBuffers();
}

// create a double buffered 500x500 pixel color window
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Transforms: Lab 3");
	glEnable(GL_DEPTH_TEST);
	glutDisplayFunc(display);
   glutReshapeFunc(reshapeCallback);
   glutKeyboardFunc(keyCallback);
   glutMouseFunc(mouseCallback);
	glutMainLoop();
	return 0;
}

//---------------------------------------------------------
//  Utility functions

void normalize(float v[3]) {
   // normalize v[] and return the result in v[]
   // from OpenGL Programming Guide, p. 58
   GLfloat d = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
   if (d == 0.0) {
      printf("zero length vector");
      return;
   }
   v[0] = v[0]/d; v[1] = v[1]/d; v[2] = v[2]/d;
}

void normCrossProd(float v1[3], float v2[3], float out[3]) {
   // cross v1[] and v2[] and return the result in out[]
   // from OpenGL Programming Guide, p. 58
   out[0] = v1[1]*v2[2] - v1[2]*v2[1];
   out[1] = v1[2]*v2[0] - v1[0]*v2[2];
   out[2] = v1[0]*v2[1] - v1[1]*v2[0];
   normalize(out);
}

