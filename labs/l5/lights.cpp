/*
 * Christopher Patton
 * CPE471, Lab 5
 *
 * Arrange so that there are two spotlights on the right side,
 * at the same location, both pointing up and a little off to
 * the side at an angle.
 *  - One spotlight should be very narrow and the other larger.
 *  - One should be white and the other green.
 *
 * Why is the spotlight jagged ("jaggies" is a technical term
 * in CG)?  Fix it.
 *
 * Place four point lights near the front of the sphere, all different
 * colors, one near each quadrant.
 *
 * Change the directional light so it shines on the bottom of
 * the sphere and make it red.
 */

#include <GL/glut.h>
#include <math.h>

// colors
static float red[]      = {0.25, 0.0,  0.0,  1.0};
static float yellow[]   = {0.25, 0.25, 0.0,  1.0};
static float green[]    = {0.0,  0.4,  0.0,  1.0};
static float cyan[]     = {0.0,  0.25, 0.25, 1.0};
static float blue[]     = {0.0,  0.0,  0.5,  1.0};
static float magenta[]  = {0.25, 0.0,  0.25, 1.0};
static float white[]    = {0.25, 0.25, 0.25, 1.0};

// spotlights
static float spot_0_pos[] = {-2.0, 0.0, -0.5};
static float spot_0_dir[] = {-0.5, 0.0, 0.0};
static float spot_0_angle = 30;
static float spot_0_exp   = 1;
static float spot_1_pos[] = {-2.0, 0.0, -1.0};
static float spot_1_dir[] = {-0.5, 0.0, 0.0};
static float spot_1_angle = 10;
static float spot_1_exp   = 1;

// directional light
static float direc_pos[] = {0.0, -1.0, 0.0, 0.0};

// point lights
static float point_0_pos[] = { 1.0,  1.0,  1.0};
static float point_1_pos[] = {-1.0,  1.0,  1.0};
static float point_2_pos[] = {-1.0, -1.0,  1.0};
static float point_3_pos[] = { 1.0, -1.0,  1.0};

void setUpView();
void setUpLight();
void reshape(int w, int h);
void display();

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Lab5: Lights");

   glEnable(GL_DEPTH_TEST);
   glEnable(GL_LIGHTING);
   glEnable(GL_COLOR_MATERIAL);

   glShadeModel(GL_SMOOTH);

   glEnable(GL_LIGHT0);
   glEnable(GL_LIGHT1);
   glEnable(GL_LIGHT2);
   glEnable(GL_LIGHT3);
   glEnable(GL_LIGHT4);
   glEnable(GL_LIGHT5);
   glEnable(GL_LIGHT6);

   glutDisplayFunc(display);
   glutReshapeFunc(reshape);

   glutMainLoop();

   return 0;
}

void setUpView() {
   glLoadIdentity();
   gluLookAt(0.0, 0.0, 0.0,
             0.0, 0.0, -1.0,
             0.0, 1.0, 0.0
   );
}
void setUpLight() {
   // spotlights
   glLightfv(GL_LIGHT0, GL_DIFFUSE, green);
   glLightfv(GL_LIGHT0, GL_POSITION, spot_0_pos);
   glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_0_dir);
   glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, spot_0_angle);
   glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, spot_0_exp);

   glLightfv(GL_LIGHT1, GL_DIFFUSE, white);
   glLightfv(GL_LIGHT1, GL_POSITION, spot_1_pos);
   glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spot_1_dir);
   glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, spot_1_angle);
   glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, spot_1_exp);

   // directional light
   glLightfv(GL_LIGHT2, GL_DIFFUSE, red);
   glLightfv(GL_LIGHT2, GL_POSITION, direc_pos);

   // point lights
   glLightfv(GL_LIGHT3, GL_DIFFUSE, magenta);
   glLightfv(GL_LIGHT3, GL_POSITION, point_0_pos);

   glLightfv(GL_LIGHT4, GL_DIFFUSE, blue);
   glLightfv(GL_LIGHT4, GL_POSITION, point_1_pos);

   glLightfv(GL_LIGHT5, GL_DIFFUSE, green);
   glLightfv(GL_LIGHT5, GL_POSITION, point_2_pos);

   glLightfv(GL_LIGHT6, GL_DIFFUSE, yellow);
   glLightfv(GL_LIGHT6, GL_POSITION, point_3_pos);

}
void reshape(int w, int h) {
   glViewport(0, 0, w, h);

   glMatrixMode(GL_PROJECTION);

   glLoadIdentity();
   gluPerspective(60.0, 1.0, 0.1, 50.0);

   glMatrixMode(GL_MODELVIEW);

   glutPostRedisplay();
}
void display() {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   setUpView();

   glPushMatrix();
   glTranslatef(0, 0, -10);
   setUpLight();
   glutSolidSphere(2, 200, 200);
   glPopMatrix();

   glutSwapBuffers();
}
