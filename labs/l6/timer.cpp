/*
 *  Lab exercise 7.0
 *  CPE 471, Computer Graphics
 *  Copyright 2005 Chris Buckalew
 *
/*--------------------------------------------------------------
 *  This lab demonstrates the glutTimer function which is used for
 *  timing events.  Initially it just cycles a stoplight -
 *  2 seconds for red and 4 seconds for green.  A Don't Walk
 *  light is also present and flashing.
 *
 *  Make the following modifications:
 *
 *  1) Add a yellow light and include it in the rotation - 1 second of
 *     yellow between green and red
 *
 *  2) Add a Walk light (make it white) and include Walk/Don't Walk
 *     in the rotation - Walk when the
 *     light is green and Don't Walk when it is red or yellow.
 *     Don't worry about when the Don't Walk light flashes
 *
 *  3) Now before somebody gets run over, adjust the timing so that the
 *     Don't Walk light comes on halfway through the green light.
 *
 *  4) When the Don't Walk light comes on, make it flash on and
 *     off twice a second by using another timer that times the
 *     flashing.  The code is already written using another
 *     timer callback function - you just have to make sure it turns
 *     on and off at the right times.
 *
 *  5) When you're done with all that, add a keypress callback
 *     so that the Walk light is only activated when the light
 *     turns green after the "w" key is pressed; the Don't Walk
 *     light remains on all through the cycle otherwise.  Make
 *     sure that the user can press the "w" key at any time,
 *     and the Walk light will come on during the next full
 *     green light.  If the user presses the "w" key during a
 *     green nothing happens until the next green.
 *
 *  When you're done everything should work just like a real
 *  stoplight.  If it doesn't you need to fix it!
 *
 *------------------------------------------------------------
 */


#ifdef __APPLE__
#include "GLUT/glut.h"
#include <OPENGL/gl.h>
#endif
#ifdef __unix__
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <stdio.h>
//#include "GLSL_helper.h"
#include <math.h>

// some function prototypes
void display(void);
void normalize(float[3]);
void normCrossProd(float[3], float[3], float[3]);

void timeStep(int step);
void walkStep(int step);
void flashStep(int step);

// initial viewer position
static GLdouble modelTrans[] = {0.0, 0.0, -5.0};
// initial model angle
static GLfloat theta[] = {0.0, 0.0, 0.0};
static float thetaIncr = 5.0;

const int GREEN_LIGHT = 1;
const int RED_LIGHT = 2;
const int YELLOW_LIGHT = 3;
int lightState = RED_LIGHT;
bool slowState = false;
bool flashState = true;
bool walkState = false;
bool walkRequested = false;
bool walkGranted = false;

//---------------------------------------------------------
//   Set up the view

void setUpView() {
   // this code initializes the viewing transform
   glLoadIdentity();

   // moves viewer along coordinate axes
   gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

   // move the view back some relative to viewer[] position
   glTranslatef(0.0f,0.0f, -8.0f);

   // rotates view
   glRotatef(0, 1.0, 0.0, 0.0);
   glRotatef(0, 0.0, 1.0, 0.0);
   glRotatef(0, 0.0, 0.0, 1.0);

   return;
}

//----------------------------------------------------------
//  Set up model transform

void setUpModelTransform() {

   // moves model along coordinate axes
   glTranslatef(modelTrans[0], modelTrans[1], modelTrans[2]);

   // rotates model
   glRotatef(theta[0], 1.0, 0.0, 0.0);
   glRotatef(theta[1], 0.0, 1.0, 0.0);
   glRotatef(theta[2], 0.0, 0.0, 1.0);


}

//----------------------------------------------------------
//  Set up the light

void setUpLight() {
   // set up the light sources for the scene
   // a directional light source to serve as a headlight
   GLfloat lightDir[] = {0.0, 0.0, 5.0, 0.0};
   GLfloat diffuseComp[] = {1.0, 1.0, 1.0, 1.0};

   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glLightfv(GL_LIGHT0, GL_POSITION, lightDir);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseComp);

   return;
}

//--------------------------------------------------------
//  Set up the objects

void drawCube(float xSize, float ySize, float zSize) {

   // save the transformation state
   glPushMatrix();

   // locate it in the scene
   glMatrixMode(GL_MODELVIEW);
   // note that center of cube is at origin
   glTranslatef(0, 0, 0);
   glScalef(xSize, ySize, zSize);

   // draw the cube - the parameter is the length of the sides
   glutSolidCube(2.0);

   // recover the transform state
   glPopMatrix();

   return;
}

void drawTrafficLight() {

   GLfloat redLightColor[4];
   redLightColor[1] = 0; redLightColor[2] = 0; redLightColor[3] = 1.0;
   GLfloat yellowLightColor[4];
   yellowLightColor[2] = 0; yellowLightColor[3] = 1.0;
   GLfloat greenLightColor[4];
   greenLightColor[0] = 0; greenLightColor[2] = 0; greenLightColor[3] = 1.0;
   GLfloat walkLightColor[4];
   walkLightColor[3] = 1.0;
   GLfloat dontWalkLightColor[4];
   dontWalkLightColor[2] = 0; dontWalkLightColor[3] = 1.0;

   if (lightState == RED_LIGHT) {
      greenLightColor[1] = yellowLightColor[0] = yellowLightColor[1] = 0.4;
      redLightColor[0] = 1;
   }
   else if (lightState == YELLOW_LIGHT) {
      redLightColor[0] = greenLightColor[1] = 0.4;
      yellowLightColor[0] = yellowLightColor[1] = 1.0;
   }
   else if (lightState == GREEN_LIGHT) {
      redLightColor[0] = yellowLightColor[0] = yellowLightColor[1] = 0.4;
      greenLightColor[1] = 1.0;
   }

   if (walkState) {
      if (slowState) {
         if (flashState) {
            walkLightColor[0] = walkLightColor[1] = walkLightColor[2] = 0.4;
            dontWalkLightColor[0] = 1.0;
            dontWalkLightColor[1] = 0.6;
         } else {
            walkLightColor[0] = walkLightColor[1] = walkLightColor[2] = 0.4;
            dontWalkLightColor[0] = dontWalkLightColor[1] = 0.4;
         }
      } else {
         walkLightColor[0] = walkLightColor[1] = walkLightColor[2] = 1.0;
         dontWalkLightColor[0] = dontWalkLightColor[1] = 0.4;
      }
   } else {
      walkLightColor[0] = walkLightColor[1] = walkLightColor[2] = 0.4;
      dontWalkLightColor[0] = 1.0;
      dontWalkLightColor[1] = 0.6;
   }

   glPushMatrix();
      glColor3fv(redLightColor);
      glTranslatef(-3, 3, 0);
      drawCube(1, 1, 0.1);

      glColor3fv(yellowLightColor);
      glTranslatef(0, -2, 0);
      drawCube(1, 1, 0.1);

      glColor3fv(greenLightColor);
      glTranslatef(0, -2, 0);
      drawCube(1, 1, 0.1);

      glColor3fv(walkLightColor);
      glTranslatef(0, -4, 0);
      drawCube(2, 1, 0.1);

      glColor3fv(dontWalkLightColor);
      glTranslatef(0, -2, 0);
      drawCube(2, 1, 0.1);
   glPopMatrix();

}

//-----------------------------------------------------------
//  Callback functions

void reshapeCallback(int w, int h) {
   // from Angel, p.562

   glViewport(0,0,w,h);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   if (w < h) {
      glFrustum(-2.0, 2.0, -2.0*(GLfloat) h / (GLfloat) w,
                2.0*(GLfloat) h / (GLfloat) w, 2.0, 20.0);
   }
   else {
      glFrustum(-2.0, 2.0, -2.0*(GLfloat) w / (GLfloat) h,
                2.0*(GLfloat) w / (GLfloat) h, 2.0, 20.0);
   }

   glMatrixMode(GL_MODELVIEW);
}

void timeStep(int step) {
   if (lightState == RED_LIGHT) {
      walkGranted = walkRequested;
      lightState = GREEN_LIGHT;
      walkStep(0);
      glutTimerFunc(2000, walkStep, 0);
      glutTimerFunc(4000, timeStep, 0);
   }
   else if (lightState == YELLOW_LIGHT) {
      lightState = RED_LIGHT;
      glutTimerFunc(2000, timeStep, 0);
   }
   else if (lightState == GREEN_LIGHT) {
      lightState = YELLOW_LIGHT;
      glutTimerFunc(1000, timeStep, 0);
   }
   display();
}

void walkStep(int step) {
   if (walkState) {
      walkState = false;
   } else if (walkGranted) {
      walkState = true;
      walkRequested = false;
   }
}

void flashStep(int step) {
   int delay = 250;
   if (flashState) {
      flashState = false;
      glutTimerFunc(delay, flashStep, 0);
   } else {
      flashState = true;
      glutTimerFunc(delay, flashStep, 0);
   }
   display();
}

void mouseCallback(int button, int state, int x, int y) {
   // rotate camera
   GLint axis = 3;
   if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) axis = 0;
   if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) axis = 1;
   if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) axis = 2;
   if (axis < 3) {  // button ups won't change axis value from 3
      theta[axis] += thetaIncr;
      if (theta[axis] > 360.0) theta[axis] -= 360.0;
      display();
   }
}

void keyCallback(unsigned char key, int x, int y) {

   // move viewer with x, y, and z keys
   // capital moves in + direction, lower-case - direction
   switch (key) {
      case ' ': walkRequested = true; break;
      case 'x': modelTrans[0] -= 1.0; break;
      case 'X': modelTrans[0] += 1.0; break;
      case 'y': modelTrans[1] -= 1.0; break;
      case 'Y': modelTrans[1] += 1.0; break;
      case 'z': modelTrans[2] -= 1.0; break;
      case 'Z': modelTrans[2] += 1.0; break;
      case 'r': theta[0] = theta[1] = theta[2] = 0.0; break;
      case '-': thetaIncr = -thetaIncr; break;
      case '+': if (thetaIncr < 0) thetaIncr--; else thetaIncr++; break;
   }

   display();
}


//---------------------------------------------------------
//  Main routines

void display (void) {
   // this code executes whenever the window is redrawn (when opened,
   //   moved, resized, etc.
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   // set the viewing transform
   setUpView();

   // set up light source
   //setUpLight();

   // start drawing objects
   setUpModelTransform();
   drawTrafficLight();

   glutSwapBuffers();
}

// create a double buffered 500x500 pixel color window
int main(int argc, char* argv[]) {
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
   glutInitWindowSize(500, 500);
   glutInitWindowPosition(100, 100);
   glutCreateWindow("Timer Lab: Lab 7");
   glEnable(GL_DEPTH_TEST);
   glutDisplayFunc(display);
        glutReshapeFunc(reshapeCallback);
        glutKeyboardFunc(keyCallback);
        glutMouseFunc(mouseCallback);
   glutTimerFunc(2000,timeStep, 2000);  // 2 second callback
   glutTimerFunc(500,flashStep, 500);  // 1/2 second callback
   glutMainLoop();
   return 0;
}

//---------------------------------------------------------
//  Utility functions

void normalize(float v[3]) {
   // normalize v[] and return the result in v[]
   // from OpenGL Programming Guide, p. 58
   GLfloat d = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
   if (d == 0.0) {
      printf("zero length vector");
      return;
   }
   v[0] = v[0]/d; v[1] = v[1]/d; v[2] = v[2]/d;
}

void normCrossProd(float v1[3], float v2[3], float out[3]) {
   // cross v1[] and v2[] and return the result in out[]
   // from OpenGL Programming Guide, p. 58
   out[0] = v1[1]*v2[2] - v1[2]*v2[1];
   out[1] = v1[2]*v2[0] - v1[0]*v2[2];
   out[2] = v1[0]*v2[1] - v1[1]*v2[0];
   normalize(out);
}

