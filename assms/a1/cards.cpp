/*
 * Christopher Patton
 * CPE471, Assignment 1
 *
 * Create a simple OpenGL  model that contains at least four primitives and at
 * least four materials (colors).  This is just to get your feet wet with
 * OpenGL, so you don't need to get fancy.  But you'll have to do more than just
 * a few shapes in a row.  In the past, people have made interesting clocks,
 * motorcycles, and robots, just to name a few things that can be simple yet
 * interesting.  Look in the GLUT documentation to see what primitives are
 * available (or look in glut.h).  The documentation is available at opengl.org.
 */

#ifdef __APPLE__
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#endif
#ifdef __unix__
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// some function prototypes
void display(void);
void normalize(float[3]);
void normCrossProd(float[3], float[3], float[3]);

// initial viewer position
static GLdouble modelTrans[] = {0.0, 0.0, -5.0};
// initial model angle
static GLfloat theta[] = {0.0, 0.0, 0.0};
static float thetaIncr = 5.0;

// animation transform variables
static GLdouble translate[3] = {-10.0, 0.0, 0.0};

//---------------------------------------------------------
//   Set up the view

void setUpView() {
   // this code initializes the viewing transform
   glLoadIdentity();

   // moves viewer along coordinate axes
   gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

   // move the view back some relative to viewer[] position
   glTranslatef(0.0f,0.0f, -8.0f);

   // rotates view
   glRotatef(0, 1.0, 0.0, 0.0);
   glRotatef(0, 0.0, 1.0, 0.0);
   glRotatef(0, 0.0, 0.0, 1.0);

   return;
}

//----------------------------------------------------------
//  Set up model transform

void setUpModelTransform() {

   // moves model along coordinate axes
   glTranslatef(modelTrans[0], modelTrans[1], modelTrans[2]);

   // rotates model
   glRotatef(theta[0], 1.0, 0.0, 0.0);
   glRotatef(theta[1], 0.0, 1.0, 0.0);
   glRotatef(theta[2], 0.0, 0.0, 1.0);


}

//----------------------------------------------------------
//  Set up the light

void setUpLight() {
   // set up the light sources for the scene
   // a directional light source from over the right shoulder
   GLfloat lightDir[] = {0.0, 0.0, 5.0, 0.0};
   GLfloat diffuseComp[] = {1.0, 1.0, 1.0, 1.0};

   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glLightfv(GL_LIGHT0, GL_POSITION, lightDir);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseComp);

   return;
}

//--------------------------------------------------------
//  Set up the objects

float white[] = {1.0, 1.0, 1.0, 1.0};
float black[] = {0.0, 0.0, 0.0, 1.0};
float red[] = {1.0, 0.0, 0.0, 1.0};
float green[] = {0.0, 1.0, 0.0, 1.0};
float blue[] = {0.0, 0.0, 1.0, 1.0};

/**
 * Draw a rectangular card with dimensions 4.0x6.0x0.1, centered at the origin.
 */
void drawCard() {
   glPushMatrix();

   glMaterialfv(GL_FRONT, GL_DIFFUSE, white);
   glMatrixMode(GL_MODELVIEW);
   glScalef(4.0, 6.0, 0.1);
   glutSolidCube(1.0);

   glPopMatrix();
}

/**
 * Draw a spade, composed of:
 *  - a cone at top
 *  - two spheres at bottom corners
 *  - a rectangle at bottom center
 */
void drawSpade() {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, black);
   glMatrixMode(GL_MODELVIEW);

   glPushMatrix();
   glTranslatef(0.0, 0.5, 0.0);
   glRotatef(90.0, -1.0, 0.0, 0.0);
   glScalef(1.5, 1.5, 1.5);
   glutSolidCone(1.0, 1.0, 10, 10);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-0.5, 0.5, 0.0);
   glScalef(1.0, 0.5, 1.0);
   glutSolidSphere(1.0, 10, 10);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0.5, 0.5, 0.0);
   glScalef(1.0, 0.5, 1.0);
   glutSolidSphere(1.0, 10, 10);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0.0, 0.0, 0.0);
   glScalef(0.5, 1.0, 1.0);
   glutSolidCube(1.0);
   glPopMatrix();
}
/**
 * Draw five spades: one large at center, four small at corners.
 */
void drawSpadeCard() {
   drawCard();

   glPushMatrix();
   glTranslatef(0.0, -0.5, 0.1);

   glPushMatrix();
   glTranslatef(1.5, 2.5, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawSpade();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-1.5, 2.5, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawSpade();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-1.5, -2.0, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawSpade();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(1.5, -2.0, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawSpade();
   glPopMatrix();

   glPushMatrix();
   glScalef(0.75, 0.75, 0.1);
   drawSpade();
   glPopMatrix();

   glPopMatrix();
}

/**
 * Draw a heart, composed of:
 *  - a cone at bottom
 *  - two spheres at top corners
 */
void drawHeart() {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, red);
   glMatrixMode(GL_MODELVIEW);

   glPushMatrix();
   glTranslatef(0.0, 0.5, 0.0);
   glRotatef(90.0, 1.0, 0.0, 0.0);
   glScalef(1.5, 1.5, 2.0);
   glutSolidCone(1.0, 1.0, 10, 10);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-0.5, 0.5, 0.0);
   glScalef(1.0, 1.0, 1.0);
   glutSolidSphere(1.0, 10, 10);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0.5, 0.5, 0.0);
   glScalef(1.0, 1.0, 1.0);
   glutSolidSphere(1.0, 10, 10);
   glPopMatrix();
}
/**
 * Draw five hearts: one large at center, four small at corners.
 */
void drawHeartCard() {
   drawCard();

   glPushMatrix();
   glTranslatef(0.0, -0.25, 0.1);

   glPushMatrix();
   glTranslatef(1.5, 2.5, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawHeart();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-1.5, 2.5, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawHeart();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-1.5, -2.0, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawHeart();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(1.5, -2.0, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawHeart();
   glPopMatrix();

   glPushMatrix();
   glScalef(0.75, 0.75, 0.1);
   drawHeart();
   glPopMatrix();

   glPopMatrix();
}

/**
 * Draw a club, composed of:
 *  - one torus at top
 *  - two torii at sides
 *  - one rectangle at bottom center
 */
void drawClub() {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);
   glMatrixMode(GL_MODELVIEW);

   glPushMatrix();
   glTranslatef(0.0, 1.0, 0.0);
   glScalef(1.0, 1.0, 1.0);
   glutSolidTorus(0.25, 0.75, 10, 10);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-0.75, 0.25, 0.0);
   glScalef(1.0, 1.0, 1.0);
   glutSolidTorus(0.25, 0.75, 10, 10);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0.75, 0.25, 0.0);
   glScalef(1.0, 1.0, 1.0);
   glutSolidTorus(0.25, 0.75, 10, 10);
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0.0, -0.5, 0.0);
   glScalef(0.5, 2.0, 1.0);
   glutSolidCube(1.0);
   glPopMatrix();
}
/**
 * Draw five clubs: one large at center, four small at corners.
 */
void drawClubCard() {
   drawCard();

   glPushMatrix();
   glTranslatef(0.0, -0.25, 0.1);

   glPushMatrix();
   glTranslatef(1.5, 2.5, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawClub();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-1.5, 2.5, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawClub();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-1.5, -2.0, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawClub();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(1.5, -2.0, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawClub();
   glPopMatrix();

   glPushMatrix();
   glScalef(0.75, 0.75, 0.1);
   drawClub();
   glPopMatrix();

   glPopMatrix();
}

/**
 * Draw a diamond, composed of:
 *  - one cube rotated 45 degrees
 */
void drawDiamond() {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, green);
   glMatrixMode(GL_MODELVIEW);

   glPushMatrix();
   glTranslatef(0.0, 0.0, 0.0);
   glRotatef(45.0, 0.0, 0.0, 1.0);
   glScalef(2.0, 2.0, 2.0);
   glutSolidCube(1.0);
   glPopMatrix();
}
/**
 * Draw five diamonds: one large at center, four small at corners.
 */
void drawDiamondCard() {
   drawCard();

   glPushMatrix();
   glTranslatef(0.0, -0.25, 0.1);

   glPushMatrix();
   glTranslatef(1.5, 2.5, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawDiamond();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-1.5, 2.5, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawDiamond();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-1.5, -2.0, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawDiamond();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(1.5, -2.0, 0.0);
   glScalef(0.25, 0.25, 0.1);
   drawDiamond();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0.0, 0.25, 0.0);
   glScalef(0.75, 0.75, 0.1);
   drawDiamond();
   glPopMatrix();

   glPopMatrix();
}

/**
 * Draw the following cards (clockwise, beginning at the top left):
 *  - spade
 *  - heart
 *  - club
 *  - diamond
 *
 * ... such that there is a 0.2 wide gap between all cards.
 *
 * All shapes are flattened (scaled in z by 0.1) in their respective functions.
 */
void drawCards() {
   glPushMatrix();
   glTranslatef(-2.1, 3.1, 0);   // top left
   drawSpadeCard();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(2.1, 3.1, 0);    // top right
   drawHeartCard();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(2.1, -3.1, 0);   // bottom right
   drawClubCard();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(-2.1, -3.1, 0);  // bottom left
   drawDiamondCard();
   glPopMatrix();
}

//-----------------------------------------------------------
//  Callback functions

void reshapeCallback(int w, int h) {
   // from Angel, p.562

   glViewport(0,0,w,h);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   if (w < h) {
      glFrustum(-2.0, 2.0, -2.0*(GLfloat) h / (GLfloat) w,
                2.0*(GLfloat) h / (GLfloat) w, 2.0, 200.0);
   }
   else {
      glFrustum(-2.0, 2.0, -2.0*(GLfloat) w / (GLfloat) h,
                2.0*(GLfloat) w / (GLfloat) h, 2.0, 200.0);
   }

   glMatrixMode(GL_MODELVIEW);
}

void mouseCallback(int button, int state, int x, int y) {
   // rotate camera
   GLint axis = 3;
   if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) axis = 0;
   if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) axis = 1;
   if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) axis = 2;
   if (axis < 3) {  // button ups won't change axis value from 3
      theta[axis] += thetaIncr;
      if (theta[axis] > 360.0) theta[axis] -= 360.0;
      display();
   }
}

void keyCallback(unsigned char key, int x, int y) {
   // move viewer with x, y, and z keys
   // capital moves in + direction, lower-case - direction
   if (key == 'x') modelTrans[0] -= 1.0;
   if (key == 'X') modelTrans[0] += 1.0;
   if (key == 'y') modelTrans[1] -= 1.0;
   if (key == 'Y') modelTrans[1] += 1.0;
   if (key == 'z') modelTrans[2] -= 1.0;
   if (key == 'Z') modelTrans[2] += 1.0;
   if (key == 'r') {
      theta[0] = 0.0; theta[1] = 0.0; theta[2] = 0.0;
   }
   if (key == '-') {
      thetaIncr = -thetaIncr;
   }
   if (key == '+') {
      if (thetaIncr < 0) thetaIncr = thetaIncr - 1.0;
      else               thetaIncr = thetaIncr + 1.0;
   }
   display();
}


//---------------------------------------------------------
//  Main routines

void display (void) {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   setUpView();
   setUpLight();
   setUpModelTransform();

   drawCards();

   glutSwapBuffers();
}

// create a double buffered 500x500 pixel color window
int main(int argc, char* argv[]) {
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
   glutInitWindowSize(500, 500);
   glutInitWindowPosition(100, 100);
   glutCreateWindow("Shapes: Assm 1");
   glEnable(GL_DEPTH_TEST);
   glutDisplayFunc(display);
   glutReshapeFunc(reshapeCallback);
   glutKeyboardFunc(keyCallback);
   glutMouseFunc(mouseCallback);
   glutMainLoop();
   return 0;
}

//---------------------------------------------------------
//  Utility functions

void normalize(float v[3]) {
   // normalize v[] and return the result in v[]
   // from OpenGL Programming Guide, p. 58
   GLfloat d = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
   if (d == 0.0) {
      printf("zero length vector");
      return;
   }
   v[0] = v[0]/d; v[1] = v[1]/d; v[2] = v[2]/d;
}

void normCrossProd(float v1[3], float v2[3], float out[3]) {
   // cross v1[] and v2[] and return the result in out[]
   // from OpenGL Programming Guide, p. 58
   out[0] = v1[1]*v2[2] - v1[2]*v2[1];
   out[1] = v1[2]*v2[0] - v1[0]*v2[2];
   out[2] = v1[0]*v2[1] - v1[1]*v2[0];
   normalize(out);
}

