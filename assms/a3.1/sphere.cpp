/*
 * Christopher Patton
 * CPE471, Assignment 3.1
 *
 * This is the first part of a two-part assignment.  For this program, your
 * program must generate the vertices and normals for a sphere.  Your sphere
 * must be in the form of a face set or an indexed face set, with a normal
 * associated with each vertex.  The reason for this is that you will use this
 * sphere in the next assignment when you compute the illumination for all the
 * sphere vertices, for which you'll need the vertex values and the normal
 * values.  Here are some hints for getting this job done:
 *
 * 1) If you build the sphere at the origin with radius 1, then you can use sine
 * and cosine functions to generate the coordinate values directly.
 *
 * 2) Model the sphere using latitude-longitude slices.  This way, the sphere is
 * composed of a number of horizontal slices (representing arcs of latitude)
 * from the South Pole to the North Pole.  Some useful formulas - if phi goes
 * from -90 to +90, then the circle at latitude phi has radius fabs(cos(phi)).
 * Try it out: cos(-90), at the South Pole, has the value 0, so the circle of
 * latitude at the South Pole has radius 0.  At the equator, cos(0) has the
 * value 1, so the circle of latitude at the equator has radius 1.  For each
 * circle of latitude, the x and z values vary according to the sine and cosine
 * of the angle of longitude, call it theta, scaled by the radius of the circle
 * of latitude.  Theta varies between 0 and 360.
 *
 * 3) The hardest part of this assignment is generating the vertex values in the
 * right order, and building the GL_QUADS in the right order.  I recommend a
 * double-nested loop which generates all the vertices along a line of longitude
 * from the South Pole to the North Pole in the inner loop, and all the lines of
 * latitude from 0 to 360 in the outer loop.  Fill a matrix of the proper size
 * with these vertices.  It's extremely likely that your first attempt will be
 * incorrect - in this case use small values for the number of lines of latitude
 * and longtitude (say 8 and 8) and print out the values of x, y, and z for each
 * vertex generated.  Make sure that your y values are increasing from -1 to 1
 * for each line of longitude, and your x and z values look reasonable for that
 * line along the surface of the sphere.  Get this part right before you go on
 * to the next part.
 *
 * I recommend using an indexed face set to generate the sphere.  Store all the
 * vertices in one line of longitude in a row in the array, from south to north,
 * then all the vertices in the next line of longitude in the array, from south
 * to north, and so on.  Now you have to create the array of indices for this
 * face set.  If you have 8 lines of longitude, each with 8 vertices, in your
 * array, then you have to hook together vertices 0, 1, 9, and 8 to make the
 * first polygon, 1, 2, 10, and 9 to make the second polygon, and so on.
 * Naturally you'll need a double-nested loop to get this done.  Store these
 * integer values in the index array.
 *
 * 4) Now creating the GL_QUADS is pretty easy.  You can start out with code
 * identical to the code that generated the GL_QUADS in the first lab.  For the
 * loop inside the glBegin(GL_QUADS) block, just change the limit on the loop
 * from six to the number of quads you're generating.
 *
 * 5) Once you have a sphere on the screen, you can improve its appearance by
 * using a glNormal3fv( ) call before each glVertex3fv( ) call.  Since this is a
 * unit sphere, the normal at each point is the same as the point on the surface
 * of the sphere.  Try it, and your sphere will look a lot smoother.  THIS IS
 * JUST LUCKY!  ORDINARILY A VERTEX'S NORMAL HAS NO CORRELATION WITH ITS
 * POSITION!  THE UNIT SPHERE IS THE ONLY OBJECT IN THE UNIVERSE FOR WHICH THIS
 * IS TRUE!
 */

#include <GL/glut.h>
#include <math.h>

static float light_dir[] = {0.0, 0.0, 1.0, 0.0};
static float light_comp[] = {1.0, 1.0, 1.0, 1.0};
static float blue[] = {0.0, 0.0, 1.0, 1.0};

static float detail = 1.0;

void setUpView();
void setUpLight();
void reshape(int w, int h);
void keyboard(unsigned char key, int x, int y);
void display();

void drawSphere();

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Assignment 3.1: Sphere");

   glEnable(GL_DEPTH_TEST);
   glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);

   glutMainLoop();

   return 0;
}

void setUpView() {
   glLoadIdentity();
   gluLookAt(0.0, 0.0, 0.0,
             0.0, 0.0, -1.0,
             0.0, 1.0, 0.0
   );
}
void setUpLight() {
   glLightfv(GL_LIGHT0, GL_POSITION, light_dir);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, light_comp);
}
void reshape(int w, int h) {
   glViewport(0, 0, w, h);

   glMatrixMode(GL_PROJECTION);

   glLoadIdentity();
   gluPerspective(60.0, 1.0, 0.1, 50.0);

   glMatrixMode(GL_MODELVIEW);

   glutPostRedisplay();
}
void keyboard(unsigned char key, int x, int y) {
   switch (key) {
      case '=': case '+':
         detail *= 2.0;
         break;
      case '-': case '_':
         detail /= 2.0;
         break;
      case 'h': case 'H':
         detail = 1.0;
         break;
   }

   glutPostRedisplay();
}
void display() {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   setUpView();
   setUpLight();

   glPushMatrix();
   glTranslatef(0, 0, -2.2);
   drawSphere();
   glPopMatrix();

   glutSwapBuffers();
}

void drawVertex(float x, float y, float z) {
   float v[3] = {x, y, z};
   glNormal3fv(v);
   glVertex3fv(v);
}
void drawSphere() {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);

   glMatrixMode(GL_MODELVIEW);

   int slices, stacks;
   slices = stacks = detail * 8;

   glBegin(GL_QUADS);
   for (int ndx1 = 0; ndx1 < slices; ++ndx1) {
      float theta1 = ndx1 * 2 * M_PI / (float)slices,
            theta2 = (ndx1 + 1) * 2 * M_PI / (float)slices,
            cos_th1 = cos(theta1),
            cos_th2 = cos(theta2),
            sin_th1 = sin(theta1),
            sin_th2 = sin(theta2);

      for (int ndx2 = 0; ndx2 < stacks; ++ndx2) {
         float phi1 = ndx2 * M_PI / (float)stacks,
               phi2 = (ndx2 + 1) * M_PI / (float)stacks,
               cos_ph1 = cos(phi1),
               cos_ph2 = cos(phi2),
               sin_ph1 = sin(phi1),
               sin_ph2 = sin(phi2);

         // bottom left
         drawVertex(sin_th1 * cos_ph1, sin_th1 * sin_ph1, cos_th1);
         // bottom right
         drawVertex(sin_th2 * cos_ph1, sin_th2 * sin_ph1, cos_th2);
         // top right
         drawVertex(sin_th2 * cos_ph2, sin_th2 * sin_ph2, cos_th2);
         // top left
         drawVertex(sin_th1 * cos_ph2, sin_th1 * sin_ph2, cos_th1);
      }
   }
   glEnd();
}
