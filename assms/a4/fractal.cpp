/*
 * Christopher Patton
 * CPE471, Assignment 4: Rendering A Fractal Landscape
 *
 * Write a OpenGL program that generates fractal landscapes.  Two important
 * constants you'll need are the ruggedness (R) and the iteration depth.
 * Initially for debugging purposes you can compile these constants in.
 *
 * Some hints: for a recursion depth of N, create an array of floats that is
 * (2^N) + 1 square.  This array will contain only y values.  Let the x and z
 * values be 0 and 1 at the corners.  Now the x and z values associated with the
 * midpoint (array location [2^(N-1)][2^(N-1)]) are 0.5 and 0.5.  The x and z
 * values are not stored in this array; they're implicit in your algorithm.  Now
 * assign y values to the four corner points.  Use the expression we came up
 * with in class to get the midpoints along each edge and the middle point.  Now
 * the original polygon has been broken into four polygons - recurse on each of
 * the four in the same fashion until the appropriate depth has been reached.
 *
 * Once the array is filled with y values, create another array to contain the
 * vertex coordinates for the indexed face set.  This array is the usual
 * single-dimensional array filled with x, y, and z values for each coordinate.
 * I recommend using an indexed face set just as in the sphere assignment to
 * make coloring the surface easier.
 *
 * Once everything is running, adjust the ruggedness with keystrokes: use 'k'
 * and 'l' multiply the current ruggedness by 0.5 and 2.0.
 *
 * This basic code will get you 70 points.  For 15 more points, do altitude
 * shading (certain height ranges get certain colors).  To make this easier you
 * must use OpenGL illumination rather than your own illumination from the last
 * assignment.  Just set the color array to the right altitude color for each
 * vertex, and set a glMaterial( ) call along with a glNormal( ) call in front
 * of each vertex and use the vertex color within the glMaterial( ) call:
 *
 * glMaterialfv(GL_FRONT, GL_DIFFUSE, &sphereColors[index]);
 *
 * You can also make it look cooler by creating flat "lakes".
 *
 * For 15 more points, control the recursion depth with keystrokes:  'h' and 'j'
 * increment or decrement the iteration depth.  It may be easier to do this by
 * fully populating the matrix down to the max level of recursion, then use the
 * appropriate entries to draw a landscape with more or fewer polygons.
 *
 *
 * More helpful info:
 *
 * Here's how I would design the recursive method - of course, there's a lot
 * missing here that you'll have to add, like the base case.
 *
 * private void recurse(xMin, xMax, zMin, zMax, sideLength) {
 *    // this function divides a polygon into four smaller polygons and
 *    // recursively calls itself
 *    // we already know the four corner points
 *
 *    // first calculate the four midpoints of each of the edges in the
 *    // way we talked about in class
 *
 *    // now get the center point - now we know nine points, which are
 *    // all the corners of the four sub-polygons
 *
 *    // now recurse on each of the four polygons
 *    recurse(xMin, (xMin+xMax)/2, zMin, (zMin+zMax)/2, sideLength/2);
 *    recurse((xMin+xMax)/2, xMax, zMin, (zMin+zMax)/2, sideLength/2);
 *    recurse(xMin, (xMin+xMax)/2, (zMin+zMax)/2, zMax, sideLength/2);
 *    recurse((xMin+xMax)/2, xMax, (zMin+zMax)/2, zMax, sideLength/2);
 * }
 *
 * Random numbers are generated with the rand( ) function - this returns a
 * random positive integer between 0 and RAND_MAX.  To get a random number
 * between -1 and 1, divide rand( ) by RAND_MAX, multiply by 2 and subtract 1.
 * Don't do integer divides!  Reseeding the random number generator, if you want
 * to do that, is with the srand( ) function.
 *
 * You'll need to generate the normal by crossing two polygon edges, just as in
 * the first lab.
 *
 * This program will be due Wednesday Nov. 14th.  No late demos, and I don't
 * need a printout.
 */

#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MIN_DEPTH 1
#define MAX_DEPTH 128
#define DFLT_DEPTH 8
#define DFLT_RUGGEDNESS 0.25

static float modelTrans[3];
static float theta[3];
static float thetaIncr = 5.0;

float light_dir[4] = {0.0, 0.0, 1.0, 0.0},
      white[4] = {1.0, 1.0, 1.0, 1.0},
      red[4] = {1.0, 0.0, 0.0, 1.0},
      green[4] = {0.0, 1.0, 0.0, 1.0},
      blue[4] = {0.0, 0.0, 1.0, 1.0};

int depth = DFLT_DEPTH;
float ruggedness = DFLT_RUGGEDNESS;
float vertices[MAX_DEPTH + 1][MAX_DEPTH + 1][3];
bool vertex_track[MAX_DEPTH + 1][MAX_DEPTH + 1];

void setUpView();
void setUpLight();
void setUpModel();
void reshape(int w, int h);
void keyboard(unsigned char key, int x, int y);
void mouse(int button, int state, int x, int y);
void display();
void reset();

void buildVertex(int x, int y, float h, int level);
void buildPolygon(int x1, int x2, int y1, int y2, int level);
void buildLandscape();
void initLandscape();
void drawVertex(int x, int y);
void drawLandscape();
void printVertex(int x, int y);
void printLandscape();

static inline float dot_prod(float* v1, float* v2) {
   return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}
static inline float magnitude(float* v) {
   return sqrt(dot_prod(v, v));
}
static inline void scale(float* v, float a) {
   for (int ndx = 0; ndx < 3; ++ndx) {
      v[ndx] *= a;
   }
}
static inline void normalize(float* v) {
   float mag = magnitude(v);

   if (mag != 0) {
      scale(v, 1 / mag);
   }
}
static inline void cross_prod(float* v1, float* v2, float* v3) {
   // Sarrus' Rule for 3x3 determinants
   v3[0] = v1[1] * v2[2] - v1[2] * v2[1];
   v3[0] = v1[2] * v2[0] - v1[0] * v2[2];
   v3[0] = v1[0] * v2[1] - v1[1] * v2[0];
}
static inline float cosine(float* v1, float* v2) {
   return dot_prod(v1, v2) / (magnitude(v1), magnitude(v2));
}
static inline float sine(float* v1, float* v2) {
   register float cos_theta = cosine(v1, v2);
   return sqrt(1 - (cos_theta * cos_theta));
}
static inline void add(float* v1, float* v2, float* v3) {
   for (int ndx = 0; ndx < 3; ++ndx) {
      v3[ndx] = v1[ndx] + v2[ndx];
   }
}
static inline void subtract(float* v1, float* v2, float* v3) {
   for (int ndx = 0; ndx < 3; ++ndx) {
      v3[ndx] = v1[ndx] - v2[ndx];
   }
}
static inline void print_vector(float* v) {
   printf("[%.2f %.2f %.2f]", v[0], v[1], v[2]);
}

inline float unit_rand() {
   volatile int r = rand();
   return (r / (double)RAND_MAX) * 2.0 - 1.0;
}

void setUpView() {
   glLoadIdentity();
   gluLookAt(0.0,  0.0,  0.0, // center
             0.0,  0.0, -1.0, // forward
             0.0,  1.0,  0.0  // up
   );
}
void setUpLight() {
   glLightfv(GL_LIGHT0, GL_POSITION, light_dir);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, white);
}
void setUpModel() {
   glTranslatef(modelTrans[0], modelTrans[1], modelTrans[2]);
   glRotatef(theta[0], 1.0, 0.0, 0.0);
   glRotatef(theta[1], 0.0, 1.0, 0.0);
   glRotatef(theta[2], 0.0, 0.0, 1.0);
}
void reshape(int w, int h) {
   glViewport(0, 0, w, h);

   glMatrixMode(GL_PROJECTION);

   glLoadIdentity();
   gluPerspective(60.0, 1.0, 0.1, 50.0);

   glMatrixMode(GL_MODELVIEW);

   glutPostRedisplay();
}
void keyboard(unsigned char key, int x, int y) {
   switch (key) {
      case 'h': depth = (depth >= MAX_DEPTH) ? depth : depth * 2; break;
      case 'j': depth = (depth <= MIN_DEPTH) ? depth : depth / 2; break;
      case 'k': ruggedness *= 0.5; break;
      case 'l': ruggedness *= 2.0; break;

      case 'x': modelTrans[0] -= 1.0; break;
      case 'X': modelTrans[0] += 1.0; break;
      case 'y': modelTrans[1] -= 1.0; break;
      case 'Y': modelTrans[1] += 1.0; break;
      case 'z': modelTrans[2] -= 1.0; break;
      case 'Z': modelTrans[2] += 1.0; break;
      case '+': thetaIncr += (thetaIncr < 0) ? -1 : 1; break;
      case '-': thetaIncr = -thetaIncr; break;

      case 'r': reset(); break;
   }

   glutPostRedisplay();
}
void mouse(int button, int state, int x, int y) {
   int axis = 3;

   if (state == GLUT_DOWN) {
      switch (button) {
         case GLUT_LEFT_BUTTON: axis = 0; break;
         case GLUT_MIDDLE_BUTTON: axis = 1; break;
         case GLUT_RIGHT_BUTTON: axis = 2; break;
      }
   }

   if (axis < 3) {
      theta[axis] += thetaIncr;
      if (theta[axis] > 360.0) theta[axis] -= 360.0;
   }

   glutPostRedisplay();
}
void display() {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   setUpView();
   setUpLight();
   setUpModel();

   glPushMatrix();
   glTranslatef(-0.5, -0.5, 0.0);
   drawLandscape();
   glPopMatrix();

   glutSwapBuffers();
}

int main(int argc, char** argv) {
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
   glutInitWindowSize(500, 500);
   glutInitWindowPosition(100, 100);
   glutCreateWindow("Fractal Landscape: Assm 4");

   glEnable(GL_DEPTH_TEST);
   glEnable(GL_COLOR_MATERIAL);
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   buildLandscape();

   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutMouseFunc(mouse);

   reset();
   glutMainLoop();

   return 0;
}

void reset() {
   modelTrans[0] = 0;
   modelTrans[1] = 0;
   modelTrans[2] = -2.5;
   theta[0] = theta[2] = -45.0;
   theta[1] = 0.0;
   thetaIncr = 5.0;

   depth = DFLT_DEPTH;
   ruggedness = DFLT_RUGGEDNESS;
}

void buildVertex(int x, int y, float h, int level) {
   if (!vertex_track[x][y]) {
      float factor = 1 / pow(2, level),
            sum = h * factor,
            count = 1.0;
      int offset = MAX_DEPTH / level;

      if (x > offset && vertex_track[x - offset][y]) {
         sum += vertices[x - offset][y][2];
         ++count;
      } else if (x < MAX_DEPTH + 1 - offset && vertex_track[x + offset][y]) {
         sum += vertices[x + offset][y][2];
         ++count;
      }

      if (y > offset && vertex_track[x][y - offset]) {
         sum += vertices[x][y - offset][2];
         ++count;
      } else if (y < MAX_DEPTH + 1 - offset && vertex_track[x][y + offset]) {
         sum += vertices[x][y + offset][2];
         ++count;
      }

      vertices[x][y][2] = sum / count;
      vertex_track[x][y] = true;
   }
}
void buildPolygon(int x1, int x2, int y1, int y2, int level) {
   buildVertex(x1, y1, unit_rand(), level);
   buildVertex(x2, y1, unit_rand(), level);
   buildVertex(x1, y2, unit_rand(), level);
   buildVertex(x2, y2, unit_rand(), level);

   if (x2 - x1 > 1 && y2 - y1 > 1) {
      int x3 = x1 + (x2 - x1) / 2,
          y3 = y1 + (y2 - y1) / 2;
      int next_level = level + 1;

      buildPolygon(x1, x3, y1, y3, next_level);
      buildPolygon(x3, x2, y1, y3, next_level);
      buildPolygon(x1, x3, y3, y2, next_level);
      buildPolygon(x3, x2, y3, y2, next_level);
   }
}
void buildLandscape() {
   srand(time(NULL));
   initLandscape();
   buildPolygon(0, MAX_DEPTH, 0, MAX_DEPTH, 1.0);
}
void initLandscape() {
   for (int ndx1 = 0; ndx1 < MAX_DEPTH + 1; ++ndx1) {
      for (int ndx2 = 0; ndx2 < MAX_DEPTH + 1; ++ndx2) {
         vertices[ndx1][ndx2][0] = ndx1 / (float)MAX_DEPTH;
         vertices[ndx1][ndx2][1] = ndx2 / (float)MAX_DEPTH;
         vertices[ndx1][ndx2][2] = FLT_MIN;

         vertex_track[ndx1][ndx2] = false;
      }
   }

   vertices[0][0][2] = 0.0;
   vertex_track[0][0] = true;
   vertices[0][MAX_DEPTH][2] = 0.0;
   vertex_track[0][MAX_DEPTH] = true;
   vertices[MAX_DEPTH][0][2] = 0.0;
   vertex_track[MAX_DEPTH][0] = true;
   vertices[MAX_DEPTH][MAX_DEPTH][2] = 0.0;
   vertex_track[MAX_DEPTH][MAX_DEPTH] = true;
}

void getCrossVector(int x1, int y1, int x2, int y2, float* v) {
   x1 = std::max(x1, 0);
   y1 = std::max(y1, 0);
   x2 = std::min(x2, MAX_DEPTH);
   y2 = std::min(y2, MAX_DEPTH);

   subtract(vertices[x2][y2], vertices[x1][y1], v);
   v[2] *= ruggedness;
   normalize(v);
}
float* getColor(int x, int y) {
   x = std::max(0, std::min(MAX_DEPTH, x));
   y = std::max(0, std::min(MAX_DEPTH, y));
   float h = vertices[x][y][2];

   if (h < 0.0) {
      return blue;
   } else if (h < 0.2) {
      return green;
   } else if (h < 0.4) {
      return red;
   } else {
      return white;
   }
}
void drawVertex(int x, int y) {
   int offset = MAX_DEPTH / depth;
   float* color = getColor(x, y);
   float v1[3], v2[3], v[3], n[3];

   v[0] = vertices[x][y][0];
   v[1] = vertices[x][y][1];
   v[2] = std::max(vertices[x][y][2] * ruggedness, 0.0f);

   getCrossVector(x - offset, y, x + offset, y, v1);
   getCrossVector(x, y - offset, x, y + offset, v2);
   cross_prod(v1, v2, n);
   normalize(n);

   /* glMaterialfv(GL_FRONT, GL_DIFFUSE, color); */
   glColor3fv(color);
   glNormal3fv(n);
   glVertex3fv(v);
}
void drawLandscape() {
   glMatrixMode(GL_MODELVIEW);

   glBegin(GL_QUADS);

   int offset = MAX_DEPTH / depth;
   for (int ndx1 = 0; ndx1 <= MAX_DEPTH - offset; ndx1 += offset) {
      for (int ndx2 = 0; ndx2 <= MAX_DEPTH - offset; ndx2 += offset) {
         drawVertex(ndx1, ndx2);
         drawVertex(ndx1, ndx2 + offset);
         drawVertex(ndx1 + offset, ndx2 + offset);
         drawVertex(ndx1 + offset, ndx2);
      }
   }

   glEnd();
}

void printVertex(int x, int y) {
   print_vector(vertices[x][y]);
}
void printLandscape() {
   for (int ndx1 = 0; ndx1 < MAX_DEPTH + 1; ++ndx1) {
      for (int ndx2 = 0; ndx2 < MAX_DEPTH + 1; ++ndx2) {
         printVertex(ndx1, ndx2);
         printf(" ");
      }
      printf("\n");
   }
}
