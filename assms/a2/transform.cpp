/*
 * Christopher Patton
 * CPE471, Assignment 2
 *
 * Write an OpenGL program that allows the user to enter a standard transform
 * that will be applied to an OpenGL model.  Use the console window, printf, and
 * scanf to prompt the user and get input.
 */

#include <GL/glut.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define MAIN_PROMPT ">Please enter a transform command: (t)ranslate, (s)cale, (r)otate, (c)oncatenate, (a)pply, or (h)ome:"
#define TRANSLATE_PROMPT ">Please enter the translation amounts (x, y, and z):"
#define SCALE_PROMPT ">Please enter the scale amounts (x, y, and z):"
#define ROTATE_PROMPT ">Please enter the rotation angle and axis:"

static float light_dir[] = {0.0, 0.0, 1.0, 0.0};
static float light_comp[] = {1.0, 1.0, 1.0, 1.0};
static float blue[] = {0.0, 0.0, 1.0, 1.0};
static float current_matrix[16];
static float master_matrix[16];
static bool concat = false;

void callback();
void identity(float* m);
void translate();
void scale();
void rotate();
void concatenate();
void apply();
void multiply(float* m1, float* m2, float* m3);

void setUpView();
void setUpLight();
void applyMatrix();
void reshape(int w, int h);
void keyboard(unsigned char key, int x, int y);
void display();

void drawObjs();

int main(int argc, char** argv) {
   identity(master_matrix);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Assignment 2: Transforms");

   glEnable(GL_DEPTH_TEST);
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);

   glutMainLoop();

   return 0;
}

void callback() {
   char option;
   do {
      printf("%s ", MAIN_PROMPT);
      scanf(" %c", &option);

      switch (option) {
         case 't':
            translate();
            break;
         case 's':
            scale();
            break;
         case 'r':
            rotate();
            break;
         case 'c':
            concatenate();
            break;
         case 'a':
            apply();
            break;
         case 'h':
            identity(master_matrix);
            identity(current_matrix);
            break;
         default:
            break;
      }
   } while (option != 't' && option != 's' && option != 'r' &&
    option != 'c' && option != 'a' && option != 'h');
}
void identity(float* m) {
   for (int ndx = 0; ndx < 16; ++ndx) {
      m[ndx] = ((ndx / 4) == (ndx % 4)) ? 1.0 : 0.0;
   }
}
void translate() {
   if (!concat) {
      identity(current_matrix);
   }

   float x, y, z;

   do {
      printf("%s ", TRANSLATE_PROMPT);
   } while (scanf(" %f %f %f", &x, &y, &z) != 3);

   float m1[16];
   float m2[16];

   for (int ndx = 0; ndx < 16; ++ndx) {
      m1[ndx] = current_matrix[ndx];
   }

   identity(m2);
   m2[12] = x;
   m2[13] = y;
   m2[14] = z;

   multiply(m1, m2, current_matrix);

   concat = false;
}
void scale() {
   if (!concat) {
      identity(current_matrix);
   }

   float x, y, z;

   do {
      printf("%s ", SCALE_PROMPT);
   } while (scanf(" %f %f %f", &x, &y, &z) != 3);

   float m1[16];
   float m2[16];

   for (int ndx = 0; ndx < 16; ++ndx) {
      m1[ndx] = current_matrix[ndx];
   }

   identity(m2);
   m2[0] = x;
   m2[5] = y;
   m2[10] = z;

   multiply(m1, m2, current_matrix);

   concat = false;
}
void rotate() {
   if (!concat) {
      identity(current_matrix);
   }

   float angle;
   char axis;

   do {
      printf("%s ", ROTATE_PROMPT);
   } while ((scanf(" %f %c", &angle, &axis) != 2) &&
    (axis != 'x' && axis != 'y' && axis !='z'));

   float m1[16];
   float m2[16];
   float sin_th = sin(angle * M_PI / 180);
   float cos_th = cos(angle * M_PI / 180);

   for (int ndx = 0; ndx < 16; ++ndx) {
      m1[ndx] = current_matrix[ndx];
   }

   identity(m2);
   switch (axis) {
      case 'x':
         m2[5] = cos_th;
         m2[6] = -sin_th;
         m2[9] = sin_th;
         m2[10] = cos_th;
         break;
      case 'y':
         m2[0] = cos_th;
         m2[2] = sin_th;
         m2[8] = -sin_th;
         m2[10] = cos_th;
         break;
      case 'z':
         m2[0] = cos_th;
         m2[1] = sin_th;
         m2[4] = -sin_th;
         m2[5] = cos_th;
         break;
      default:
         break;
   }

   multiply(m1, m2, current_matrix);

   concat = false;
}
void concatenate() {
   concat = true;
}
void apply() {
   float m[16];

   for (int ndx = 0; ndx < 16; ++ndx) {
      m[ndx] = master_matrix[ndx];
   }

   multiply(m, current_matrix, master_matrix);
}
void multiply(float* m1, float* m2, float* m3) {
   for (int ndx1 = 0; ndx1 < 4; ++ndx1) {
      for (int ndx2 = 0; ndx2 < 4; ++ndx2) {
         float val = 0;

         for (int ndx3 = 0; ndx3 < 4; ++ndx3) {
            val += m1[ndx1 * 4 + ndx3] * m2[ndx3 * 4 + ndx2];
         }

         m3[ndx1 * 4 + ndx2] = val;
      }
   }
}

void setUpView() {
   glLoadIdentity();
   gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}
void setUpLight() {
   glLightfv(GL_LIGHT0, GL_POSITION, light_dir);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, light_comp);
}
void applyMatrix() {
   glMultMatrixf(master_matrix);
}
void reshape(int w, int h) {
   glViewport(0, 0, w, h);

   glMatrixMode(GL_PROJECTION);

   glLoadIdentity();
   gluPerspective(60.0, 1.0, 0.1, 50.0);

   glMatrixMode(GL_MODELVIEW);

   glutPostRedisplay();
}
void keyboard(unsigned char key, int x, int y) {
   callback();
   glutPostRedisplay();
}
void display() {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   setUpView();
   setUpLight();

   glPushMatrix();
   applyMatrix();
   drawObjs();
   glPopMatrix();

   glutSwapBuffers();
}

void drawObjs() {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, blue);

   glMatrixMode(GL_MODELVIEW);

   glTranslatef(0, 0, 1);
   glutSolidCube(2.0);
}
