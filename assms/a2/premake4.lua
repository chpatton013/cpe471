solution ('Assm2')
   configurations ({'Debug', 'Release'})
   language ('C++')
   files ({'./**.cpp', './**.cc', './**.h'})
   defines ({'GL_GLEXT_PROTOTYPES'})
   links ({'m', 'GL', 'GLU', 'glut'})

   configuration ('Debug')
      defines ({'DEBUG'})
      flags ({'Symbols'})

   configuration ('Release')
      defines ({'NDEBUG'})
      flags ({'Optimize'})

   project ('assm2')
      kind ('ConsoleApp')
