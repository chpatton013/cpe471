/*
 * Christopher Patton
 * CPE471, Assignment 3.2
 *
 * 1) Turn off OpenGL lighting by commenting out the setUpLight( ) function and
 * the glMaterialfv( ) call.  Now the screen should be blank.  To get something
 * on the screen, make a call to glColor3f(1.0, 1.0, 1.0).  Now you should see
 * the sphere again, but it looks flat - this is because all the vertices are
 * exactly the same color and there is no shading with respect to any light
 * sources.
 *
 * 2) The next thing to do is to set up an array of colors that parallels the
 * array of vertices.  This will be an array of floating point rgb values, each
 * triple of which corresponds to the vertex in the same location in the vertex
 * array.  Now this color array can be indexed just like the vertex array. Load
 * this array with, say, white for each vertex, or try a random value (between 0
 * and 1) for each of the rgb values.  To get these colors onto the screen, set
 * the color just before making the vertex call:
 *    glColor3fv(&sphereColors[sphereIndex[i]*3]);
 *    glVertex3fv(&sphereVerts[sphereIndex[i]*3]);
 *
 * Now the sphere is still flat looking and white, but each vertex has its own
 * white color.  Put some random colors in there and see what the effect is.
 *
 * 3) To get some shading to the sphere, try coloring the vertices with the
 * diffuse component of the Illumination Equation - color of the light times the
 * diffuse reflectance of the object times the dot product of the light vector
 * and the normal vector.  Since you are controlling the lighting and
 * reflectance, there are no OpenGL calls to do this.  Define a light color (say
 * white), a diffuse reflectance color (say red) and a vector to the light (say
 * <1  1  1>, but normalized of course).  Now when you're computing and storing
 * the vertices, compute and store the color at the same time by dotting the
 * light vector with the normal vector, which happens to be the same as the
 * vertex value (and it's even already normalized), then multiplying that by the
 * light and the reflectance rgb.  Store the resulting rbg value in the color
 * array.  When it's working, your sphere should be nicely diffusely shaded.
 *
 * 4) Now for a specular component.  Again, you'll need the light vector but
 * this time you also need a reflectance vector.  (Remember, you need to dot
 * this vector with the vector to the viewer, which for our purposes we can say
 * is  <0  0  1> for the camera location we've been using.)  Here is the formula
 * for the reflectance vector - it is non-obvious but correct:
 *    R = 2*(N dot L)*N - L
 * where N is the normal vector and L is the light vector.  What this means is
 * the x component of the reflectance vector is 2 times the dot product of N and
 * L times the x component of the normal vector minus the x component of the
 * light vector.  Once you have this vector, dot it with the viewing vector and
 * raise that to some power m.  Multiply the result times the light rgb and the
 * specular reflectance (which is a scalar, remember).  Add that final result to
 * the diffuse component and you have the total color for that vertex.
 *
 * Some caveats: when testing your specular, turn off the diffuse completely so
 * you don't get distracted by diffuse effects.  Don't forget that if NdotL is
 * negative, that means that you're on the wrong side and there should be NO
 * specular contribution - this is the usual cause of spots of light on the dark
 * side of the sphere.
 *
 * 5) Now add a keystroke callback to allow the user to enter the light vector
 * instead of compiling it in as a constant.  Be sure to normalize it as well.
 * Also allow the user to enter the specular power.
 */

#include <GL/glut.h>
#include <math.h>
#include <stdio.h>

static float view_direction[3] = {0.0, 0.0, 1.0};
static float light_direction[3] = {1.0, 1.0, 0.5};
static float diffuse_light[3] = {1.0, 1.0, 1.0};
static float diffuse_comp[3] = {0.0, 0.0, 1.0};
static float specular_light[3] = {1.0, 1.0, 1.0};
static float specular_comp = 1.0;
static float specular_gloss = 8.0;
static float detail = 20.0;

void setUpView();
void move_light();
void reshape(int w, int h);
void keyboard(unsigned char key, int x, int y);
void display();

void drawSphere();

static inline float dot_prod(float* v1, float* v2) {
   return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}
static inline float magnitude(float* v) {
   return sqrt(dot_prod(v, v));
}
static inline void scale(float* v, float a) {
   for (int ndx = 0; ndx < 3; ++ndx) {
      v[ndx] *= a;
   }
}
static inline void normalize(float* v) {
   float mag = magnitude(v);

   if (mag != 0) {
      scale(v, 1 / mag);
   }
}
static inline float cosine(float* v1, float* v2) {
   return dot_prod(v1, v2) / (magnitude(v1), magnitude(v2));
}
static inline float sine(float* v1, float* v2) {
   register float cos_theta = cosine(v1, v2);
   return sqrt(1 - (cos_theta * cos_theta));
}
static inline void add(float* v1, float* v2, float* v3) {
   for (int ndx = 0; ndx < 3; ++ndx) {
      v3[ndx] = v1[ndx] + v2[ndx];
   }
}
static inline void subtract(float* v1, float* v2, float* v3) {
   for (int ndx = 0; ndx < 3; ++ndx) {
      v3[ndx] = v1[ndx] - v2[ndx];
   }
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Assignment 3.2: Lighting");

   glEnable(GL_DEPTH_TEST);

   normalize(view_direction);
   normalize(light_direction);

   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);

   glutMainLoop();

   return 0;
}

void setUpView() {
   glLoadIdentity();
   gluLookAt(0.0, 0.0, 0.0,
             0.0, 0.0, -1.0,
             0.0, 1.0, 0.0
   );
}
void reshape(int w, int h) {
   glViewport(0, 0, w, h);

   glMatrixMode(GL_PROJECTION);

   glLoadIdentity();
   gluPerspective(60.0, 1.0, 0.1, 50.0);

   glMatrixMode(GL_MODELVIEW);

   glutPostRedisplay();
}
void keyboard(unsigned char key, int x, int y) {
   switch (key) {
      case '=': case '+':
         detail += 1.0;
         break;
      case '-': case '_':
         detail = (detail == 1) ? detail : detail - 1;
         break;
      case ')':
         specular_gloss += 1.0;
         break;
      case '(':
         specular_gloss = (specular_gloss == 8) ?
          specular_gloss : specular_gloss - 1;
         break;
      case 'l':
         move_light();
         break;
      case 'h': case 'H':
         detail = 20.0;
         specular_gloss = 8.0;
         light_direction[0] = 1.0;
         light_direction[1] = 1.0;
         light_direction[2] = 0.5;
         break;
   }

   glutPostRedisplay();
}
void display() {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   setUpView();

   glPushMatrix();
   glTranslatef(0, 0, -2.2);
   drawSphere();
   glPopMatrix();

   glutSwapBuffers();
}

void move_light() {
   printf("Input new light vector: ");
   scanf("%f, %f, %f",
      &(light_direction[0]),
      &(light_direction[1]),
      &(light_direction[2])
   );
   normalize(light_direction);
}

void get_diff_color(float* v, float* color) {
   float cos_theta = cosine(v, light_direction);
   for (int ndx = 0; ndx < 3; ++ndx) {
      color[ndx] = diffuse_light[ndx] * diffuse_comp[ndx] * cos_theta;
   }
}
void get_spec_color(float* v, float* color) {
   float reflect[3], scaled_normal[3];
   float NdotL = dot_prod(v, light_direction);
   float cos_phi;

   if (NdotL <= 0) {
      return;
   }

   for (int ndx = 0; ndx < 3; ++ndx) {
      scaled_normal[ndx] = v[ndx] * 2 * NdotL;
   }

   subtract(scaled_normal, light_direction, reflect);
   cos_phi = cosine(reflect, view_direction);

   for (int ndx = 0; ndx < 3; ++ndx) {
      color[ndx] = specular_light[ndx] * specular_comp *
       pow(cos_phi, specular_gloss);
   }
}
void get_color(float* v, float* color) {
   float diff_clr[3], spec_clr[3];

   get_diff_color(v, diff_clr);
   get_spec_color(v, spec_clr);
   add(diff_clr, spec_clr, color);
}
void drawVertex(float x, float y, float z) {
   float v[3] = {x, y, z},
         color[3];

   get_color(v, color);

   glNormal3fv(v);
   glColor3fv(color);
   glVertex3fv(v);
}
void drawSphere() {
   glMatrixMode(GL_MODELVIEW);

   int slices, stacks;
   slices = stacks = detail;

   glBegin(GL_QUADS);
   for (int ndx1 = 0; ndx1 < slices; ++ndx1) {
      float theta1 = ndx1 * 2 * M_PI / (float)slices,
            theta2 = (ndx1 + 1) * 2 * M_PI / (float)slices,
            cos_th1 = cos(theta1),
            cos_th2 = cos(theta2),
            sin_th1 = sin(theta1),
            sin_th2 = sin(theta2);

      for (int ndx2 = 0; ndx2 < stacks; ++ndx2) {
         float phi1 = ndx2 * M_PI / (float)stacks,
               phi2 = (ndx2 + 1) * M_PI / (float)stacks,
               cos_ph1 = cos(phi1),
               cos_ph2 = cos(phi2),
               sin_ph1 = sin(phi1),
               sin_ph2 = sin(phi2);

         // bottom left
         drawVertex(sin_th1 * cos_ph1, sin_th1 * sin_ph1, cos_th1);
         // bottom right
         drawVertex(sin_th2 * cos_ph1, sin_th2 * sin_ph1, cos_th2);
         // top right
         drawVertex(sin_th2 * cos_ph2, sin_th2 * sin_ph2, cos_th2);
         // top left
         drawVertex(sin_th1 * cos_ph2, sin_th1 * sin_ph2, cos_th1);
      }
   }
   glEnd();
}
