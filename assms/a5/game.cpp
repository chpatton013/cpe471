/*
 * Christopher Patton
 * CPE471, Assignment 5: Building an Interactive Game
 *
 * Write a OpenGL program that plays an interactive 3D game.  This is very
 * open-ended - any game, as long as it has these components: it uses a 3D model
 * and it's interactive (the user interacts with the game using OpenGL
 * selection).  It can also use keyboard interaction and animation (using timer
 * callbacks) but that's not required.
 *
 * A little more detail - you must use OpenGL selection (as mentioned above)
 * actually as part of the game - not just to turn a color off and on or
 * something.
 *
 * This program will be due Friday Nov 30.  Remember, no late demos!  And I
 * don't need a printout.
 */


#include <GL/glut.h>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <ctime>
#include <map>
#include <set>


/* Shared */
static float z_min = 1.0, z_max = 50.0, move_max = 10.0;
static float position[] = {0.0, 0.0};

static float light_dir[] = {0.0, 0.0, 1.0, 0.0},
             white[] = {1.0, 1.0, 1.0, 1.0},
             black[] = {0.1, 0.1, 0.1, 0.1},
             red[] = {1.0, 0.0, 0.0, 1.0},
             green[] = {0.0, 1.0, 0.0, 1.0},
             blue[] = {0.0, 0.0, 1.0, 1.0};
static float* curr_color,
            * light_ambient,
            * light_diffuse = white,
            * light_specular = white;
static int color_time;

void reset();
char* color_name();
void print_status();


/* OpenGL Business */
void setUpView();
void setUpLight();
void setUpModel();
void draw(GLenum mode);
void reshape(int w, int h);
void keyboard(unsigned char key, int x, int y);
void mouse(int button, int state, int x, int y);
void display();
void drawScene(GLenum mode);


/* Game Logic */
class Sphere {
   public:
      Sphere(float x, float y, float r, float v, float* c);
      bool clicked();
      bool check_click();
      void click();
      bool check_collision();
      void draw(GLenum mode);
      void move(float t);
      float pos();

   private:
      float x_pos, y_pos, z_pos, radius, velocity;
      float* color;
      bool was_clicked;
};

static int points, lives;
static std::set<Sphere*> spheres;
static std::map<unsigned int, Sphere*> sphere_map;
static int max_spheres = 16;
static float min_radius = 1.0,
             max_radius = 3.5,
             min_velocity = 10.0,
             max_velocity = 25.0;

void update(int data);


/* Shared */
void reset() {
   srand(time(NULL));

   position[0] = position[1] = 0.0;
   curr_color = red;
   color_time = 0;

   spheres.clear();
   sphere_map.clear();
   points = 0;
   lives = 3;

   print_status();
}
char* color_name(float* c) {
   if (c == red) return "red";
   else if (c == green) return "green";
   else if (c == blue) return "blue";
   else if (c == white) return "white";
   else if (c == black) return "black";
   else return "unknown";
}
void print_status() {
   printf("color: %s, lives: %d, points: %d\n", color_name(curr_color), lives, points);
}


/* OpenGL Business */
void setUpView() {
   glLoadIdentity();
   gluLookAt(0.0,  0.0,  0.0, // center
             0.0,  0.0, -1.0, // forward
             0.0,  1.0,  0.0  // up
   );
}
void setUpLight() {
   glLightfv(GL_LIGHT0, GL_POSITION, light_dir);
   glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
}
void setUpModel() {
   glTranslatef(position[0], position[1], 0.0);
}
void setUpProjection(int w, int h) {
   glViewport(0, 0, w, h);
   gluPerspective(60.0, w / (float)h, z_min, z_max);
   glMatrixMode(GL_MODELVIEW);
}
void draw(GLenum mode) {
   glMatrixMode(GL_MODELVIEW);
   setUpView();
   setUpLight();
   setUpModel();
   drawScene(mode);
}
void reshape(int w, int h) {
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();

   setUpProjection(w, h);

   glutPostRedisplay();
}
void keyboard(unsigned char key, int x, int y) {
   switch (key) {
      case 'a': case 'A':
         position[0] += (position[0] < move_max) ? 1.0 : 0.0;
         break;
      case 'd': case 'D':
         position[0] -= (position[0] > -move_max) ? 1.0 : 0.0;
         break;
      case 's': case 'S':
         position[1] += (position[1] < move_max) ? 1.0 : 0.0;
         break;
      case 'w': case 'W':
         position[1] -= (position[1] > -move_max) ? 1.0 : 0.0;
         break;
   }

   glutPostRedisplay();
}
void process_hits(int hits, unsigned int* bf) {
   Sphere* s = NULL;

   for (int ndx = 0; ndx < hits; ++ndx) {
      std::set<Sphere*>::iterator it =
       spheres.find(sphere_map[bf[4*hits + 3]]);
      if (it != spheres.end()) {
         (*it)->click();
      }
   }
}
void click(int x, int y) {
   unsigned int select_bf[512];
   int w, h, viewport[4];

   glGetIntegerv(GL_VIEWPORT, viewport);
   w = viewport[2];
   h = viewport[3];

   glSelectBuffer(512, select_bf);
   glRenderMode(GL_SELECT);

   glInitNames();
   glPushName(-1);

   glPushMatrix();
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      gluPickMatrix(x, h - y, 1.0, 1.0, viewport);
      setUpProjection(w, h);

      glMatrixMode(GL_MODELVIEW);
      draw(GL_SELECT);
   glPopMatrix();

   int hits = glRenderMode(GL_RENDER);
   process_hits(hits, select_bf);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   setUpProjection(w, h);
}
void mouse(int button, int state, int x, int y) {
   if (button == GLUT_LEFT_BUTTON) {
      if (state == GLUT_DOWN) {
         click(x, y);
      }
   }

   glutPostRedisplay();
}
void display() {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   draw(GL_RENDER);
   glutSwapBuffers();
}
void drawCorridor() {
   glMaterialfv(GL_FRONT, GL_DIFFUSE, curr_color);

   glPushMatrix();
   glTranslatef(0.0, 0.0, -z_max / 2.0);
   glScalef(2 * move_max, 2 * move_max, z_max);
   glutWireCube(1.0);
   glPopMatrix();
}
void drawScene(GLenum mode) {
   drawCorridor();

   for (std::set<Sphere*>::iterator it = spheres.begin();
    it != spheres.end(); ++it) {
      (*it)->draw(mode);
   }
}


/* Game Logic */
Sphere::Sphere(float x, float y, float r, float v, float* c) {
   x_pos = x;
   y_pos = y;
   z_pos = -z_max;
   radius = r;
   velocity = v;
   color = c;
   was_clicked = false;
}
bool Sphere::clicked() {
   return was_clicked;
}
bool Sphere::check_click() {
   if (color == black) {
      return false;
   } else {
      return (curr_color == white) || (curr_color == color);
   }
}
void Sphere::click() {
   was_clicked = true;
}
bool Sphere::check_collision() {
   volatile float dx = x_pos - position[0],
                  dy = y_pos - position[1],
                  dz = z_pos - z_min;

   return sqrt(dx * dx + dy * dy + dz * dz) > radius;
}
void Sphere::draw(GLenum mode) {
   int facets = std::min(std::max((z_pos + z_max) / 4, 6.0f), 18.0f);

   glPushMatrix();
   if (mode == GL_SELECT) {
      glLoadName((unsigned int)this);
   }
   glMaterialfv(GL_FRONT, GL_DIFFUSE, color);
   glTranslatef(x_pos, y_pos, z_pos);
   glutSolidSphere(radius, facets, facets);
   glPopMatrix();
}
void Sphere::move(float t) {
   z_pos += velocity * t;
}
float Sphere::pos() {
   return z_pos;
}

bool handle_click(Sphere* s) {
   if (s->clicked()) {
      points += s->check_click() ? 2 : -1;
      print_status();
      spheres.erase(s);
      sphere_map.erase((unsigned int)s);
      return false;
   } else {
      return true;
   }
}
bool handle_collision(Sphere* s) {
   if (!s->check_collision()) {
      --lives;
      spheres.erase(s);
      sphere_map.erase((unsigned int)s);

      if (lives <= 0) {
         reset();
      } else {
         print_status();
      }

      return false;
   } else {
      return true;
   }
}
bool handle_range(Sphere* s) {
   if (s->pos() > z_min) {
      spheres.erase(s);
      sphere_map.erase((unsigned int)s);
      return false;
   } else {
      return true;
   }
}
float* get_color() {
      int color_rand = rand() % 11;

      if (color_rand < 9) {
         if (color_rand < 3) {
            return red;
         } else if (color_rand < 6) {
            return green;
         } else {
            return blue;
         }
      } else {
         if (color_rand < 10) {
            return white;
         } else {
            return black;
         }
      }
}
void handle_color() {
   if (color_time > 0) {
      color_time -= 40;
   } else {
      curr_color = get_color();
      if (curr_color == white || curr_color == black) {
         color_time = 5000;
      } else {
         color_time = 10000;
      }
      light_ambient = curr_color;

      print_status();
   }
}
bool handle_objects() {
   if (spheres.size() < max_spheres) {
      float x = (rand() % (int)(2000 * move_max)) / 1000.0 - move_max,
            y = (rand() % (int)(2000 * move_max)) / 1000.0- move_max,
            r = (rand() % (int)(1000 * (max_radius - min_radius))) / 1000.0 + min_radius,
            v = (rand() % (int)(1000 * (max_velocity - min_velocity))) / 1000.0 + min_velocity,
          * c = get_color();
      Sphere* s = new Sphere(x, y, r, v, c);
      spheres.insert(s);
      sphere_map[(unsigned int)s] = s;
   } else {
      return false;
   }
}
void update(int data) {
   glutTimerFunc(40, update, 0);

   for (std::set<Sphere*>::iterator it = spheres.begin();
    it != spheres.end(); ++it) {
      if (handle_click(*it)) {
         if (handle_collision(*it)) {
            if (handle_range(*it)) {
               (*it)->move(40.0 / 1000.0);
            }
         }
      }
   }

   handle_color();
   handle_objects();

   glutPostRedisplay();
}


int main(int argc, char** argv) {
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
   glutInitWindowSize(1024, 576);
   glutInitWindowPosition(0, 0);
   glutCreateWindow("Interactive Game: Assm 5");

   glEnable(GL_DEPTH_TEST);
   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);

   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutMouseFunc(mouse);

   reset();
   update(0);

   glutMainLoop();

   return 0;
}
